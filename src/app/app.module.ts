import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from './services/api.service';
import { ImagesService } from './services/images.service';
import { EncryptAuthService } from './services/encrypt-auth.service';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SuiModule } from 'ng2-semantic-ui';
import { JwtModule } from '@auth0/angular-jwt';
import { EmbedVideo } from 'ngx-embed-video';
import { OwlModule } from 'ngx-owl-carousel';
import { NgxPayPalModule } from 'ngx-paypal';
import { LightboxModule } from 'ngx-lightbox';
import { ModalService } from './services/modal.service';
import { NgxEditorModule } from 'ngx-editor';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { AcceptRejectModalComponent } from './customComponents/AcceptRejectModalComponent';
import { JwtInterceptor } from './services/JwtInterceptor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { UsersComponent } from './components/users/users.component';

import { PostsComponent } from './components/posts/posts.component';
import { CreateComponent as CreatePost } from './components/posts/create/create.component';
import { UpdateComponent as UpdatePost } from './components/posts/update/update.component';
import { ListComponent as PostDashboard } from './components/posts/list/list.component';
import { ViewComponent as ViewPost } from './components/posts/view/view.component';

import { CommentsComponent } from './components/posts/view/comments/comments.component';

import { WebsiteComponent } from './components/website/website.component';
import { MainComponent } from './components/website/main/main.component';
import { AboutUsComponent } from './components/website/about-us/about-us.component';
import { HowItWorksComponent } from './components/website/how-it-works/how-it-works.component';
import { TermsConditionsComponent } from './components/website/terms-conditions/terms-conditions.component';
import { ContactComponent } from './components/website/contact/contact.component';

import { AdminsComponent } from './components/admins/admins.component';
import { AdminBannersComponent } from './components/admins/admin-banners/admin-banners.component';
import { AdminHowItWorksComponent } from './components/admins/admin-how-it-works/admin-how-it-works.component';
import { AdminAboutUsComponent } from './components/admins/admin-about-us/admin-about-us.component';
import { AdminTermsConditionsComponent } from './components/admins/admin-terms-conditions/admin-terms-conditions.component';
import { ListUsersComponent } from './components/admins/list-users/list-users.component';

import { CreateComponent } from './components/admins/admin-about-us/create/create.component';
import { ListComponent } from './components/admins/admin-about-us/list/list.component';
import { UpdateComponent } from './components/admins/admin-about-us/update/update.component';
import { CreateComponent as CreateBanners } from './components/admins/admin-banners/create/create.component';
import { ListComponent as ListBanners } from './components/admins/admin-banners/list/list.component';
import { UpdateComponent as UpdateBanners } from './components/admins/admin-banners/update/update.component';
import { CreateComponent as CreateWorks } from './components/admins/admin-how-it-works/create/create.component';
import { ListComponent as ListWorks } from './components/admins/admin-how-it-works/list/list.component';
import { UpdateComponent as UpdateWorks } from './components/admins/admin-how-it-works/update/update.component';
import { CreateComponent as CreateTerms } from './components/admins/admin-terms-conditions/create/create.component';
import { ListComponent as ListTerms } from './components/admins/admin-terms-conditions/list/list.component';
import { UpdateComponent as UpdateTerms } from './components/admins/admin-terms-conditions/update/update.component';

import { AuthComponent } from './components/auth/auth.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { ResetPwdComponent } from './components/auth/reset-pwd/reset-pwd.component';
import { ChangePwdComponent } from './components/auth/change-pwd/change-pwd.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { MessagesComponent } from './components/messages/messages.component';
import { SuccessMessageComponent } from './components/messages/success-message/success-message.component';
import { ErrorMessageComponent } from './components/messages/error-message/error-message.component';
import { StepsBreadcrumbsComponent } from './components/steps-breadcrumbs/steps-breadcrumbs.component';
import { PersonalInformationComponent } from './components/users/personal-information/personal-information.component';
import { MessagesPagesComponent } from './components/messages/messages-pages/messages-pages.component';
import { JobsReportedComponent } from './components/admins/jobs-reported/jobs-reported.component';
import { ViewComponent as ViewAbout } from './components/admins/admin-about-us/view/view.component';
import { ViewComponent as ViewBanners } from './components/admins/admin-banners/view/view.component';
import { ViewComponent as ViewWorks } from './components/admins/admin-how-it-works/view/view.component';
import { ViewComponent as ViewTerms } from './components/admins/admin-terms-conditions/view/view.component';
import { TruncatePipe } from './components/pipes/truncate.pipe';
import { WarningUsersComponent } from './components/admins/list-users/warning-users/warning-users.component';
import { ViewUsersListComponent } from './components/admins/list-users/view-users-list/view-users-list.component';
import { ConfirmMessageComponent } from './components/messages/confirm-message/confirm-message.component';
import { ListAllUsersComponent } from './components/admins/list-users/list-all-users/list-all-users.component';
import { ViewJobReportedComponent } from './components/admins/jobs-reported/view-job-reported/view-job-reported.component';
import { ListAllJobsReportedComponent } from './components/admins/jobs-reported/list-all-jobs-reported/list-all-jobs-reported.component';
import { BidsComponent } from './components/bids/bids.component';
import { MyBidsComponent } from './components/bids/my-bids/my-bids.component';
import { EndBidsComponent } from './components/bids/end-bids/end-bids.component';
import { ProjectsBidsComponent } from './components/bids/projects-bids/projects-bids.component';
import { EndProjectsBidsComponent } from './components/bids/end-projects-bids/end-projects-bids.component';
import { ViewAllBidsComponent } from './components/bids/my-bids/view-all-bids/view-all-bids.component';
import { MakeBidComponent } from './components/bids/my-bids/make-bid/make-bid.component';
import { ViewAllEndBidsComponent } from './components/bids/end-bids/view-all-end-bids/view-all-end-bids.component';
import { ViewAllProjectsBidsComponent } from './components/bids/projects-bids/view-all-projects-bids/view-all-projects-bids.component';
import { ViewAllEndProjectsBidsComponent } from './components/bids/end-projects-bids/view-all-end-projects-bids/view-all-end-projects-bids.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { ViewOffersFinishedComponent } from './components/bids/end-projects-bids/view-offers-finished/view-offers-finished.component';
import { ViewBidsPostsComponent } from './components/bids/my-bids/view-bids-posts/view-bids-posts.component';
import { ViewEndBidsComponent } from './components/bids/end-bids/view-end-bids/view-end-bids.component';
import { ViewAllMyBidsProjectsComponent } from './components/bids/projects-bids/view-all-my-bids-projects/view-all-my-bids-projects.component';
import { PublishComponent } from './components/posts/view/publish/publish.component';

import { SliderCarouselComponent } from './customComponents/slider-carousel/slider-carousel.component';
import { PaypalCustomComponent } from './customComponents/paypal-custom/paypal-custom.component';
import { DisplaybidsComponent } from './customComponents/displaybids/displaybids.component';
import { AdminComponent } from './components/admins/admin/admin.component';
import { WinnerCommentsComponent } from './components/posts/view/winner-comments/winner-comments.component';
import { CommentsWinnerComponent } from './components/posts/view/comments-winner/comments-winner.component';
import { ModalComponent } from './customComponents/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    WebsiteComponent,
    AdminsComponent,
    CommentsComponent,
    ContactComponent,
    AboutUsComponent,
    HowItWorksComponent,
    TermsConditionsComponent,
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    ResetPwdComponent,
    ChangePwdComponent,
    ListUsersComponent,
    AdminBannersComponent,
    AdminHowItWorksComponent,
    AdminAboutUsComponent,
    AdminTermsConditionsComponent,
    BreadcrumbsComponent,
    MessagesComponent,
    SuccessMessageComponent,
    ErrorMessageComponent,
    StepsBreadcrumbsComponent,
    MainComponent,
    CreateComponent,
    ListComponent,
    UpdateComponent,
    CreateBanners,
    ListBanners,
    UpdateBanners,
    CreateWorks,
    ListWorks,
    UpdateWorks,
    CreateTerms,
    ListTerms,
    UpdateTerms,
    PersonalInformationComponent,
    MessagesPagesComponent,
    JobsReportedComponent,
    ViewAbout,
    ViewBanners,
    ViewWorks,
    ViewTerms,
    TruncatePipe,
    WarningUsersComponent,
    ViewUsersListComponent,
    PostsComponent,
    CreatePost,
    UpdatePost,
    ViewPost,
    PostDashboard,
    ConfirmMessageComponent,
    ListAllUsersComponent,
    AcceptRejectModalComponent,
    ViewJobReportedComponent,
    ListAllJobsReportedComponent,
    BidsComponent,
    MyBidsComponent,
    EndBidsComponent,
    ProjectsBidsComponent,
    EndProjectsBidsComponent,
    ViewAllBidsComponent,
    MakeBidComponent,
    ViewAllEndBidsComponent,
    ViewAllProjectsBidsComponent,
    ViewAllEndProjectsBidsComponent,
    ErrorPageComponent,
    ViewOffersFinishedComponent,
    ViewBidsPostsComponent,
    ViewEndBidsComponent,
    ViewAllMyBidsProjectsComponent,
    PublishComponent,
    SliderCarouselComponent,
    PaypalCustomComponent,
    DisplaybidsComponent,
    AdminComponent,
    WinnerCommentsComponent,
    CommentsWinnerComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    SuiModule,
    OwlModule,
    NgxPayPalModule,
    NgxEditorModule,
    LightboxModule,
    AngularFontAwesomeModule,
    TooltipModule.forRoot(),
    JwtModule.forRoot({
      config : {
        tokenGetter: function tokenGetter() {
          return localStorage.getItem('access_token');
        },
        whitelistedDomains: [],
        blacklistedRoutes: [],
      }
    }),
    EmbedVideo.forRoot()
  ],
  providers: [ApiService, 
              EncryptAuthService, 
              AuthService, 
              AuthGuardService, 
              ImagesService,
              ModalService,
              {
                provide: HTTP_INTERCEPTORS,
                useClass: JwtInterceptor,
                multi: true
              }],
  bootstrap: [AppComponent],
  entryComponents: [AcceptRejectModalComponent]
})
export class AppModule { }
