import { Component, OnInit } from '@angular/core';
import { User } from './models/user';
import { ApiService } from './services/api.service';
import { AuthService } from './services/auth.service';
import { Router, Route } from '@angular/router';
import 'ng2-semantic-ui';
import 'jquery';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    userId   : string;
    username : string;
    admin    : any;

    constructor (private apiRest: ApiService, 
                 private auth: AuthService,
                 private _router: Router) {
    }

    ngOnInit() {
        this.username = this.auth.username;
        this.userId   = this.auth.number;
        this.admin    = this.auth.isAdmin;
    }

    /**
     * Method will redirect to the user to personal information
     * to let edit them
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    spi(): void {
        this._router.navigate([`/users/spi/${this.auth.number}`]);
    }

    /**
     * Method for logout the user from the website
     * avoid any user navigate to another restricted sites
     * with no logged in before
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    logout(): void {
        let tokens = { 'id_login' : this.auth.historyLogin,
                       'userS'    : this.auth.userT1,
                       'userL'    : this.auth.userT2,
                       'userN'    : this.auth.number };

        this.apiRest.postRequest(`/api/users/login/0/history`, JSON.stringify(tokens))
                    .subscribe(res => {});

        this.apiRest.postRequest(`/api/users/logout`, JSON.stringify(tokens))
                    .subscribe(res => {});

        this.auth.logout();

        window.location.href = '/';
    }
}
