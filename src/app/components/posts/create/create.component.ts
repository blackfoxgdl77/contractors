import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { ImagesService } from 'src/app/services/images.service';
import { IMessage } from 'ng2-semantic-ui';
import { Router, Route } from '@angular/router';
import { Post } from 'src/app/models/post';

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
    formGroupAdd : FormGroup;
    flagSuccessM : boolean = false;
    flagErrorsM  : boolean = false;
    submitedForm : boolean = false;
    enableFile   : boolean = false;
    enableUrl    : boolean = false;
    filesData    : File    = null;
    errorCode    : number  = 0;
    errorStatus  : string  = '';
    errorMessage : string  = '';

    constructor(private api: ApiService, 
                private fb: FormBuilder,
                private img: ImagesService,
                private _router: Router) { }

    ngOnInit() {
        this.validations();
    }

    /**
     * Method will be used to validate
     * the data of the forms and create a new
     * entrance
     *
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validations(): void {
        this.formGroupAdd = this.fb.group({
            publishedBy : ['', [Validators.required]],
            postname    : ['', [Validators.required]],
            description : ['', [Validators.required]],
            payment     : ['', [Validators.required]],
            city        : ['', [Validators.required]],
            category    : ['', [Validators.required]],
            options     : ['', [Validators.required]],
            status      : ['', [Validators.required]],
            urlVideo    : [''],
            file        : ['']
        });

        this.formGroupAdd.controls['file'].disable();
        this.formGroupAdd.controls['urlVideo'].disable();
    }

    /**
     * Event will be used to get the information of the 
     * image will be uploaded to the server
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    onFilesLoad(event: any) {
        this.filesData = <File>event.target.files;
    }

    /**
     * Method to check if should be enable the img to upload
     * images or url videos for save it
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    disabledEnabledFields(event: any) {
        if (event.target.value == '1') {
            this.formGroupAdd.controls['file'].enable();
            this.formGroupAdd.controls['urlVideo'].disable();
            this.formGroupAdd.controls.urlVideo.setValue('');
        } else {
            this.formGroupAdd.controls['urlVideo'].enable();
            this.formGroupAdd.controls['file'].disable();
            this.formGroupAdd.controls.file.setValue('');
        }
    }

    /**
     * Method will be used to save the new post record
     *
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    createRecord(): void {
        this.submitedForm = true;
        let values = this.formGroupAdd.value;

        if (this.formGroupAdd.invalid) {
        this.flagErrorsM = (this.flagErrorsM) ? this.flagErrorsM : !this.flagErrorsM;

        return ;
        }

        this.flagErrorsM = false;
        this.api.postRequest('/api/posts/create', JSON.stringify(values))
                .subscribe(res => {
                    if (res.body['status'] == 'OK' && res.body['code'] == 200) {
                        let dataReturned = res.body['data'];
                        this.uploadImage(dataReturned['id']);
                    }

                    if (res.body['status'] == 'ERROR' && res.body['code'] == 400) {
                        this.errorCode    = res.body['code'];
                        this.errorMessage = res.body['message'];
                        this.errorStatus  = res.body['status'];
                        this.flagErrorsM  = true;
                    }
                });
    }

    /**
     * Method will be used for upload image in every
     * section of the admin module
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    uploadImage(id): void {
        const formData = new FormData();
        formData.append('file', this.filesData[0]);
        this.img.postImage(`/api/upload/3/${id}/images`, formData)
                .subscribe(res => {
                     if (res.body['status'] == 'OK' && res.body['code'] == 200) 
                    {
                        this.flagSuccessM = true;
                    }

                    if (res.body['status'] == 'ERROR' && res.body['code'] == 400)
                    {
                        this.errorCode    = res.body['code'];
                        this.errorMessage = res.body['message'];
                        this.errorStatus  = res.body['status'];
                        this.flagErrorsM  = true;
                    }
                });
    }

    /**
     * Method will clear all the fields of the form when the user click the 
     * clear button
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearFields() : void {
        this.flagSuccessM = false;
        this.flagErrorsM  = false;
        this.errorCode    = 0;
        this.errorMessage = '';
        this.errorStatus  = '';
        this.submitedForm = false;

        this.formGroupAdd.reset();
        this.formGroupAdd.controls['file'].disable();
        this.formGroupAdd.controls['urlVideo'].disable();
        this.formGroupAdd.controls.options.setValue('');
        this.formGroupAdd.controls.publishedBy.setValue('');
        this.formGroupAdd.controls.postname.setValue('');
        this.formGroupAdd.controls.description.setValue('');
        this.formGroupAdd.controls.payment.setValue('');
        this.formGroupAdd.controls.city.setValue('');
        this.formGroupAdd.controls.category.setValue('');
        this.formGroupAdd.controls.status.setValue('');
    }

    /**
     * Method will be added for return the user once has been 
     * canceled the process of fill the form before save the data
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    cancelOption(): void {
        this._router.navigate(['/posts/dashboard']);
    }

    /**
     * Method will be used to reset values from the flag
     * to display the message again if you want to display 
     * the banner message
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message:IMessage) {
        this.flagSuccessM = false;
    }

    /**
     * Method to manipulate the form inside the template
     * easy way
     * 
     * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    get f() {
        return this.formGroupAdd.controls;
    }
}
