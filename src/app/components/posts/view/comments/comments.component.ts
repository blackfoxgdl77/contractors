import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
    @Input() publish : number;
    @Input() user    : number;
    @Input() post    : number;

    innercomment    : FormGroup;
    submitedFlag    : boolean = false;
    errorFlag       : boolean = false;
    successFlag     : boolean = false;
    codeResponse    : number;
    statusResponse  : string;
    messageResponse : string;
    headerResponse  : string;
  
    constructor(private api: ApiService,
                private auth: AuthService,
                private incomment: FormBuilder) { }

    ngOnInit() {
        this.validations();
    }

    /**
     * Validate the inner comments will be done for the users
     * that want to comment the main comment
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validations(): void {
        this.innercomment = this.incomment.group({
            comments : ['', [Validators.required, Validators.minLength(6), Validators.maxLength(200)]]
        });
    }

    /**
     * Make a inner comment that will be set as child in the main
     * comment of the post
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    makeInnerComment(): void {
        this.submitedFlag = true;
        let comment = this.innercomment.value;

        if (this.innercomment.invalid) {
            this.errorFlag = (this.errorFlag) ? this.errorFlag : !this.errorFlag;

            return ;
        }

        this.errorFlag = false;
        Object.assign(comment, { 'post'    : this.post,
                                 'publish' : this.publish });
        this.api.postRequest(`/api/make/comment`, JSON.stringify(comment))
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let returnedData = res['body'].data;

                        // TODO: create a function for set storagesession
                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));

                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.headerResponse  = res['body'].header;
                        this.successFlag     = true;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.headerResponse  = res['body'].header;
                        this.errorFlag       = false;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Clear the inner form when the user doesn't
     * want to comment the main publish
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    cancelInner(): void {
        this.innercomment.reset();

        this.errorFlag    = false;
        this.submitedFlag = false;
        this.successFlag  = false;
    }

    /**
     * Method to get the elements of the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    get f() {
        return this.innercomment.controls;
    }
}
