import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-comments-winner',
    templateUrl: './comments-winner.component.html',
    styleUrls: ['./comments-winner.component.scss']
})
export class CommentsWinnerComponent implements OnInit {
    @Input() publish : number;
    @Input() user    : number;
    @Input() post    : number;

    innerComment    : FormGroup;
    submittedFlag   : boolean = false;;
    errorFlag       : boolean = false;
    successFlag     : boolean = false;
    codeResponse    : number;
    statusResponse  : string;
    messageResponse : string;
    headerResponse  : string;

    constructor(private api: ApiService,
                private wincomment: FormBuilder) { }

    ngOnInit() {
        this.fieldValidations();
    }

    /**
     * Validate fields of the reply form
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    fieldValidations(): void {
        this.innerComment = this.wincomment.group({
            comments: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(200)]]
        });
    }

    /**
     * Validate data set in the comments and also verify that the
     * fields are correct, checking the form and sending data to store
     * comments
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    makeWinnerInnerComment() {
        this.submittedFlag = true;
        let comment = this.innerComment.value;

        if (this.innerComment.invalid) {
            this.errorFlag = (this.errorFlag) ? this.errorFlag :  !this.errorFlag;

            return ;
        }

        this.errorFlag = false;
        Object.assign(comment, { 'post'    : this.post,
                                 'publish' : this.publish });

        this.api.postRequest(`/api/make/winner/comment`, JSON.stringify(comment))
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let returnedData = res['body'].data;

                        // TODO: create a function to storage session
                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));

                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.headerResponse  = res['body'].headerResponse;
                        this.successFlag     = true;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.errorFlag       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // What code should be put here?
                });
    }

    /**
     * Clean the inner form when the user does not
     * sure that wants to make an inner comment
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    cancelInner(): void {
        this.innerComment.reset();

        this.errorFlag     = false;
        this.submittedFlag = false;
        this.successFlag   = false;
    }

    /**
     * Get elements from the form to validate data
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    get f() {
        return this.innerComment.controls;
    }

}
