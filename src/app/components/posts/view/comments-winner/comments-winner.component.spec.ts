import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsWinnerComponent } from './comments-winner.component';

describe('CommentsWinnerComponent', () => {
  let component: CommentsWinnerComponent;
  let fixture: ComponentFixture<CommentsWinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentsWinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsWinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
