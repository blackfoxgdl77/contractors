import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { IMessage } from 'ng2-semantic-ui';

@Component({
    selector: 'app-publish',
    templateUrl: './publish.component.html',
    styleUrls: ['./publish.component.scss']
})
export class PublishComponent implements OnInit {
    @Input() post     : number;
    @Input() user     : number;
    @Input() status   : number;
    @Input() username : string;

    publish         : FormGroup;
    errorFlag       : boolean = false;
    successFlag     : boolean = false;
    statusResponse  : string;
    messageResponse : string;
    headerResponse  : string;
    codeResponse    : number;
    submitedFlag    : boolean = false;
        publishes       : any     = [];
        isReply         : boolean = false;
        isReplyId       : number;

    constructor(private api: ApiService,
                private auth: AuthService,
                private publishBuilder: FormBuilder) {
                 }

    ngOnInit() {
        this.publishValidation();

        this.api.getRequest(`/api/publish/${this.post}/comments/get`)
                .subscribe((res: any) => {
                    // TODO: Implementing Observables
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let dataReturned = res['body'].data;

                        this.publishes = dataReturned;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.errorFlag       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should eb put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Will open the comments to know what is selected by the user
     * 
     * @param object evt
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    replyComment(evt: any) {
        this.isReply   = (this.isReply && this.isReplyId != evt.id) ? this.isReply : !this.isReply;
        this.isReplyId = evt.id;
    }

    /**
     * Make a request reporting the comment is offensive for the owner of the post
     * 
     * @param object evt
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    reportComment(evt: any): void {
        console.log(evt);
        let valueReport = { 'post' : this.post };

        this.api.postRequest(`/api/report/${evt.id}/fake/publish`, JSON.stringify(valueReport))
                .subscribe((res: any) => {
                    // TODO: Implementing Observables
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let returnedData = res['body'].data;

                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.headerResponse  = res['body'].header;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.errorFlag       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Will be used to report the inner comment and change the status to
     * unavailable due to is offensive for the owner of the post
     * 
     * @param evt object
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    innerComment(evt: any) : void {
        let post = {}

        this.api.postRequest(``, JSON.stringify(post))
                .subscribe((res: any) => {
                    // TODO: Implementing Observables
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let returnedData = res['body'].data;

                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.headerResponse  = res['body'].header;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.errorFlag       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Validate the form where the user should set the title of the 
     * comment over the post published by any user on the platform
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    publishValidation() : void {
        this.publish = this.publishBuilder.group({
            title   : ['', [Validators.required, Validators.minLength(6), Validators.maxLength(40)]],
            comment : ['', [Validators.required, Validators.minLength(6), Validators.maxLength(200)]]
        });
    }

    /**
     * Method will be used to save the comments will be sent
     * once the user publish a main comment in the post
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    publishComment(): void {
    }

    /**
     * Method will be used to comment the main publish for every user, I mean
     * could be the owner or any other user can comment the main publish
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    makeAComment(): void {
        this.submitedFlag = true;
        let comments      = this.publish.value;
        
        if (this.publish.invalid) {
            this.errorFlag = (this.errorFlag) ? this.errorFlag : !this.errorFlag;

            return ;
        }

        this.errorFlag = false;
        Object.assign(comments, { 'post' : this.post, 'user' : this.user });
        this.api.postRequest(`/api/publish/comment`, JSON.stringify(comments))
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let returnedData =res['body'].data;

                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));
                        
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.headerResponse  = res['body'].header;
                        this.successFlag     = true;

                        this.publish.reset();
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.messageResponse = res['body'].message;
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.errorFlag       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method will be used to cancel send the comment and clear
     * the information typed by the user
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    cancel() : void {
        this.publish.reset();

        this.errorFlag    = false;
        this.submitedFlag = false;
        this.successFlag  = false;
    }

    /**
     * Method to get the elements of the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    get f() {
        return this.publish.controls;
    }

    /**
     * Method will be used to reset values from the flag
     * to display the message again if you want to display 
     * the banner message
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message : IMessage) {
        this.successFlag  = false;
        this.errorFlag    = false;
        this.submitedFlag = false;

        this.publish.reset();
    }

    /**
     * Check the user is the owner of the post to could make specific
     * actions that wont be enabled for users wants the job
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    checkIsOwner(): boolean {
        if (this.status === 1 && this.auth.username === this.username) {
            return true;
        }

        return false;
    }
}
