import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-winner-comments',
    templateUrl: './winner-comments.component.html',
    styleUrls: ['./winner-comments.component.scss']
})
export class WinnerCommentsComponent implements OnInit {
    @Input() public post     : number;
    @Input() public status   : number;
    @Input() public username : string;
    @Input() public user     : number;

    public winner          : FormGroup;
    public errorFlag       : boolean = false;
    public successFlag     : boolean = false;
    public statusResponse  : string;
    public codeResponse    : number;
    public messageResponse : string;
    public headerResponse  : string;
    public submittedFlag   : boolean = false;
    public isReply         : boolean = false;
    public isReplyId       : boolean = false;
    public publishes       : any     = [];

    constructor(private auth: AuthService,
                private api: ApiService,
                private winnerPublish: FormBuilder) { }

    ngOnInit() {
        this.validations();

        this.api.getRequest(`/api/winner/publish/${this.post}/comments/get`)
                .subscribe((res: any) => {
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let dataReturned = res['body'].data;

                        this.publishes = dataReturned;
                        console.log("holas");
                        console.log(this.publishes);
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.errorFlag       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Get validations for all the fields will be
     * displayed on the form
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validations(): void {
        this.winner = this.winnerPublish.group({
            title   : ['', [Validators.required, Validators.minLength(6), Validators.maxLength(40)]],
            comment : ['', [Validators.required, Validators.minLength(6), Validators.maxLength(200)]]
        });
    }

    /**
     * Method used for send the comment to save to 
     * database and is going to be displayed if the publish has
     * been done successfully
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    makeAComment(): void {
        this.submittedFlag = true;
        let comment = this.winner.value;

        if (this.winner.invalid) {
            this.errorFlag = (this.errorFlag) ? this.errorFlag : !this.errorFlag;

            return ; 
        }

        this.errorFlag = false;
        Object.assign(comment, { 'postId' : this.post, 'user' : this.user });
        this.api.postRequest(`/api/winner/publish/comment`, JSON.stringify(comment))
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let returnedData =res['body'].data;

                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));
                        
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.headerResponse  = res['body'].header;
                        this.successFlag     = true;

                        this.winner.reset();
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.messageResponse = res['body'].message;
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.errorFlag       = true;
                    }
                },
                (error: any) => {
                    // TODO: what code should be put here?
                },
                () => {
                    // TODO: what code should be put here?
                });
    }

    /**
     * Method to get every element of the form
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    get f() {
        return this.winner.controls;
    }

    /**
     * Will open the inner comment to know what is the selected user
     * 
     * @param evt object
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    replyComment(evt: any): void {
        this.isReply   = (this.isReply && this.isReplyId != evt.id) ? this.isReply : !this.isReply;
        this.isReplyId = evt.id;
    }

    /**
     * Check the user is the owner of the post to could make specific
     * actions that wont be enabled for users wants the job
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    checkIsOwner(): boolean {
        if (this.status === 1 && this.auth.username === this.username) {
            return true;
        }

        return false;
    }
}
