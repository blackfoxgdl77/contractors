import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-steps-breadcrumbs',
    templateUrl: './steps-breadcrumbs.component.html',
    styleUrls: ['./steps-breadcrumbs.component.scss']
})
export class StepsBreadcrumbsComponent implements OnInit {
    @Input() steps : [];

    constructor() { }

    ngOnInit() { }
}
