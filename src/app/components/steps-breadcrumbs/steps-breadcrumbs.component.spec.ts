import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepsBreadcrumbsComponent } from './steps-breadcrumbs.component';

describe('StepsBreadcrumbsComponent', () => {
  let component: StepsBreadcrumbsComponent;
  let fixture: ComponentFixture<StepsBreadcrumbsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepsBreadcrumbsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepsBreadcrumbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
