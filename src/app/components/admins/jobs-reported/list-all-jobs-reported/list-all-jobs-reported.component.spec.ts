import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAllJobsReportedComponent } from './list-all-jobs-reported.component';

describe('ListAllJobsReportedComponent', () => {
  let component: ListAllJobsReportedComponent;
  let fixture: ComponentFixture<ListAllJobsReportedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAllJobsReportedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAllJobsReportedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
