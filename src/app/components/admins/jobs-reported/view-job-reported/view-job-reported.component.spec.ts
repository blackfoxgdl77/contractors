import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewJobReportedComponent } from './view-job-reported.component';

describe('ViewJobReportedComponent', () => {
  let component: ViewJobReportedComponent;
  let fixture: ComponentFixture<ViewJobReportedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewJobReportedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewJobReportedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
