import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsReportedComponent } from './jobs-reported.component';

describe('JobsReportedComponent', () => {
  let component: JobsReportedComponent;
  let fixture: ComponentFixture<JobsReportedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobsReportedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsReportedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
