import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { ImagesService } from 'src/app/services/images.service';
import { IMessage } from 'ng2-semantic-ui';
import { Router, Route } from '@angular/router';
import { DinamycData } from 'src/app/models/dinamyc-data';
import { Observable, Subscription } from 'rxjs';

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit, OnDestroy {
    public title  : string = "About Us / Create Record";
    public breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '/admin/administrator', isLink: true },
        { text: 'About Us - List', link: '/admin/about-us/list', isLink: true },
        { text: 'About Us - Create', link: '', isLink: false }
    ];

    // Observables and Subscriptions
    public observers  = new Observable();
    public subscriber : Subscription;

    public formGroupAdd : FormGroup;
    public flagSuccessM : boolean = false;
    public flagErrorsM  : boolean = false;
    public submitedForm : boolean = false;
    public enableFile   : boolean = false;
    public enableUrl    : boolean = false;
    public filesData    : File    = null;
    public errorCode    : number  = 0;
    public errorStatus  : string  = '';
    public errorMessage : string  = '';
    public flagCreate   : number  = 0;

    public editorConfig = {
        editable: true,
        spellcheck: true,
        height: '15rem',
        minHeight: '5rem',
        placeholder: 'Type a description...',
        translate: 'yes'
    };

    constructor(private api: ApiService, 
                private fb: FormBuilder,
                private img: ImagesService,
                private _router: Router) { }

    ngOnInit() {
        this.validations();
    }

    ngOnDestroy() {
        if (this.subscriber) {
            this.subscriber.unsubscribe();
        }
    }

    /**
     * Method will be used to validate
     * the data of the forms and create a new
     * entrance
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validations(): void {
        this.formGroupAdd = this.fb.group({
            section     : ['About Us'],
            description : ['', [Validators.required]],
            options     : ['', [Validators.required]],
            status      : ['', [Validators.required]],
            urlVideo    : [''],
            file        : ['']
        });

        this.formGroupAdd.controls['section'].disable();
        this.formGroupAdd.controls['file'].disable();
        this.formGroupAdd.controls['urlVideo'].disable();
    }

    /**
     * Event will be used to get the information of the 
     * image will be uploaded to the server
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    onFilesLoad(event: any) {
        this.filesData = <File>event.target.files;
    }

    /**
     * Method to check if should be enable the img to upload
     * images or url videos for save it
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    disabledEnabledFields(event: any) {
        if (event.target.value == '1') {
            this.formGroupAdd.controls['file'].enable();
            this.formGroupAdd.controls['urlVideo'].disable();
            this.formGroupAdd.controls.urlVideo.setValue('');
        } else {
            this.formGroupAdd.controls['urlVideo'].enable();
            this.formGroupAdd.controls['file'].disable();
            this.formGroupAdd.controls.file.setValue('');
        }
    }

    /**
     * Method will be used to save the new record related to about us
     * and will be stores on admin table
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    newRecordCreated(): void {
        this.submitedForm = true;
        let values        = this.formGroupAdd.value;
        Object.assign(values, { section : 'About Us'});

        if (values['options'] == '1') {
            Object.assign(values, { urlVideo : '' });
        }

        if (this.formGroupAdd.invalid) {
            this.flagErrorsM = (this.flagErrorsM) ? this.flagErrorsM : !this.flagErrorsM;

            return ;
        }

        this.flagErrorsM = false;
        this.observers   = this.api.postRequest('/api/admin/create', JSON.stringify(values));
        this.subscriber  = this.observers.subscribe((res: any) => {
                    if (res['body'].status == 'OK' && res['body'].code == 200) {
                        let dataReturned = res['body'].data;

                        if (dataReturned.options == '1')
                        {
                            this.uploadImage(dataReturned.id);
                        } 
                        else 
                        {
                            this.flagSuccessM = true;
                            this.flagCreate   = 1;

                            this.clearFields();
                        }
                    }

                    if (res['body'].status == 'ERROR' && res['body'].code == 400) {
                        this.errorCode    = res['body'].code;
                        this.errorMessage = res['body'].message;
                        this.errorStatus  = res['body'].status;
                        this.flagErrorsM  = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method will be used for upload image in every
     * section of the admin module
     * 
     * @param id integer
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    uploadImage(id): void {
        const formData = new FormData();
        formData.append('file', this.filesData[0]);

        this.observers  = this.img.postImage(`/api/upload/3/${id}/images`, formData);
        this.subscriber = this.observers.subscribe((res : any) => {
                    if (res['body'].status == 'OK' && res['body'].code == 200) 
                    {
                            this.flagSuccessM = true;
                            this.flagCreate   = 1;

                            this.clearFields();
                    }

                    if (res['body'].status == 'ERROR' && res['body'].code == 400)
                    {
                        this.errorCode    = res['body'].code;
                        this.errorMessage = res['body'].message;
                        this.errorStatus  = res['body'].status;
                        this.flagErrorsM  = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method will clear all the fields of the form when the user click the 
     * clear button
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearFields() : void {
        if (this.flagCreate == 0)
        {
            this.flagSuccessM = false;
        }
        
        this.flagErrorsM  = false;
        this.errorCode    = 0;
        this.errorMessage = '';
        this.errorStatus  = '';
        this.submitedForm = false;
        this.flagCreate   = 0;

        this.formGroupAdd.reset();
        this.formGroupAdd.controls['file'].disable();
        this.formGroupAdd.controls['urlVideo'].disable();
        this.formGroupAdd.controls.section.setValue('About Us');
        this.formGroupAdd.controls.options.setValue('');
        this.formGroupAdd.controls.status.setValue('');
    }

    /**
     * Method will be added for return the user once has been 
     * canceled the process of fill the form before save the data
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    cancelOption(): void {
        this._router.navigate(['/admin/about-us/list']);
    }

    /**
     * Method will be used to reset values from the flag
     * to display the message again if you want to display 
     * the banner message
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message:IMessage) {
        this.flagSuccessM = false;
        this.flagCreate   = 1;
    }

    /**
     * Method to manipulate the form inside the template
     * easy way
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    get f() {
        return this.formGroupAdd.controls;
    }
}
