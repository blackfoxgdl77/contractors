import { Component, OnInit } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { IMessage } from 'ng2-semantic-ui';
import { DinamycData } from 'src/app/models/dinamyc-data';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AcceptRejectModal } from 'src/app/helpers/AcceptRejectModal';
import { SuiModalService } from 'ng2-semantic-ui';
import { ModalService } from 'src/app/services/modal.service';
import { EmbedVideoService } from 'ngx-embed-video';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
    public title  : string = "About Us / View Record";
    public breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '/admin/administrator', isLink: true },
        { text: 'About Us - List', link: '/admin/about-us/list', isLink: true },
        { text: 'About Us - View', link: '', isLink: false }
    ];

    public formGroupView     : FormGroup;
    public aboutView         : DinamycData[];
    public paramId           : any;
    public id                : number;
    public status            : number;
    public flagDeleteRecord  : boolean = false;
    public flagSuccessRecord : boolean = false;
    public messageStatus     : string;
    public messageResponse   : string;
    public headerMessage     : string;
    public codeResponse      : number  = 0;

    public typeFile          : number;
    public fileString        : string;
    public nameString        : string;
    public videoImage        : string;

    public editorConfig = {
        editable: false,
        spellcheck: true,
        height: '15rem',
        minHeight: '5rem',
        placeholder: 'Type a description...',
        translate: 'yes'
    };


    constructor(private _router: Router,
                private fb: FormBuilder,
                private modalService: SuiModalService,
                private activateRoute: ActivatedRoute,
                private api: ApiService,
                private modal: ModalService,
                private videos: EmbedVideoService) { }

    ngOnInit() {
        this.paramId = this.activateRoute.snapshot.paramMap.get('id');

        this.api.getRequest(`/api/admin/${this.paramId}/get`)
                .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK') {
                        this.aboutView  = res.body['data'][0];
                        this.id         = this.aboutView['id'];
                        this.status     = this.aboutView['status'];
                        this.typeFile   = this.aboutView['type_file'];
                        this.fileString = (this.typeFile == 1) 
                                            ? this.aboutView['file_size']
                                            : this.videos.embed(this.aboutView['url'], { attr: { width: 800, height: 400}});
                        this.createSnapshotVideo(this.aboutView);
                        this.nameString = this.aboutView['original_name'];

                        this.fillValidations();
                    }

                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR') {
                        this.messageResponse  = res['message'];
                        this.flagDeleteRecord = true;
                        this.codeResponse     = res.body['code'];
                        this.messageStatus    = res.body['status'];
                    }
                });

        this.validationFields();
    }

    /**
     * Validate the fields, in this case should be disabled and with no
     * rules about the fields in the form, this will work due to is a 
     * function for just display the values to the admin user
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validationFields(): void
    {
        this.formGroupView = this.fb.group({
            section      : [{ value: '', disabled: true }],
            description  : [{ value: '', disabled: true }],
            options      : [{ value: '', disabled: true }],
            file         : [{ value: '', disabled: true }],
            urlVideo     : [{ value: '', disabled: true }],
            status       : [{ value: '', disabled: true }],
            date_created : [{ value: '', disabled: true }],
            date_updated : [{ value: '', disabled: true }]
        });
    }
    
    /**
     * Method used to created the snapshot from the url given as parameter
     * in the function that was created to return the html will be displayed
     * 
     * @param url object
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    createSnapshotVideo(url: object): void {
        if (url['type_file'] == 2) {
            this.videos.embed_image(url['url'], { image: 'default' })
                       .then(res => {
                           this.videoImage = res.html;
                        });
        }
    }

    /**
     * Method will fill all the fields in the view of the users
     * in admin section, where the users could enabled or disabled
     * a specific user
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    fillValidations(): void
    {
        this.formGroupView.patchValue({
            section      : this.aboutView['section'],
            description  : this.aboutView['description'],
            options      : this.aboutView['type_file'],
            //file         : this.aboutView['file'],
            urlVideo     : this.aboutView['url'],
            status       : this.aboutView['status'],
            date_created : this.aboutView['date_created'],
            date_updated : this.aboutView['date_updated']
        });
    }

    /**
     * Soft-delete the information selected by the user once clicks
     * the delete button
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    deleteRecord(title: string, message: string, id: number, button1: string, button2: string) : void {
        let modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService
            .open(modal)
            .onApprove(() => {
                this.api.deleteRequest(`/api/admin/${id}/delete`)
                    .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK') {
                        let data = res.body['data'];

                        this.flagSuccessRecord = true;
                        this.messageStatus     = res.body['status'];
                        this.messageResponse   = data['message'];
                        this.codeResponse      = res.body['code'];
                        this.headerMessage     = data['header'];
                    }

                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR') {
                        this.flagDeleteRecord = true;
                        this.messageStatus    = res.body['status'];
                        this.messageResponse  = res.body['message'];
                    }
                });
            })
            .onDeny(() => {});
    }

    /**
     * Method for active or inactive records on the admin section
     * that will be displayed on the page for all the users
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param value number
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public open(title: string, message: string, id: number, value: number, button1: string, button2: string)
    {
        const modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService
            .open(modal)
            .onApprove(() => this.modalOptions(id, value))
            .onDeny(() => {});
    }

    /**
     * Method will be used to dismiss the message displayed in the
     * banner once the user executes an actions such as delete records
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message: IMessage) {
        this.flagDeleteRecord  = false;
        this.flagSuccessRecord = false;

        this.codeResponse    = 0;
        this.messageStatus   = '';
        this.messageResponse = '';
        this.headerMessage   = '';
    }

    /**
     * Method for make a request once we get the url
     * used for make the call to the api created
     * 
     * @param id number
     * @param value number
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private modalOptions(id: number, value: number): void
    {
        let urlString = this.stringRequest(id, value);

        this.api.getRequest(urlString)
                .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK')
                    {
                        let responseData = res.body['data'];

                        this.flagSuccessRecord = true;
                        this.codeResponse      = res.body['code'];
                        this.messageStatus     = res.body['status'];
                        this.messageResponse   = responseData['message'];
                        this.headerMessage     = responseData['headerMsg']
                    }

                    if (res.body['status'] == 'ERROR' && res.body['code'] == 400)
                    {
                        this.flagDeleteRecord = true;
                        this.messageStatus    = res.body['status'];
                        this.codeResponse     = res.body['code'];
                        this.messageResponse  = res.body['message'];
                    }
                });
    }

    /**
     * Private method will be used for get the url will be
     * called the function once the modal has been approved
     * 
     * @param id number
     * @param value number
     * @return string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private stringRequest(id, value): string
    {
        let stringRequest;
        switch(value) {
            case 1: // Active
                stringRequest = `/api/availability/${id}/1/admin`;
                break;
            case 2: // Inactive
                stringRequest = `/api/availability/${id}/2/admin`;
                break;
        }

        return stringRequest;
    }

    /**
     * Method will be used to redirect the users one page back
     * once the user clicks the button
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    returnBack(): void {
        this._router.navigate(['/admin/about-us/list']);
    }

    /**
     * Method used to open the image and could see what is, this function just be able
     * to view the imaghe uploaded on the record selected
     *
     * @param id string
     * @return void 
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    viewImageOpen(id: string) : void {
        this.modal.open(id);
    }

    /**
     * Method used to close the image once the user clicks on
     * the close button
     * 
     * @param id string
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids 
     */
    viewImageClose(id: string): void {
        this.modal.close(id);
    }
}
