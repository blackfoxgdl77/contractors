import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { IMessage } from 'ng2-semantic-ui';
import { DinamycData } from 'src/app/models/dinamyc-data';
import { SuiModalService } from 'ng2-semantic-ui';
import { AcceptRejectModal } from 'src/app/helpers/AcceptRejectModal';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
    title  : string = "Terms and Conditions / List Records";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '/admin/administrator', isLink: true },
        { text: 'Terms and Conditions - List', link: '', isLink: false }
    ];

    flagDeleteRecord  : boolean = false;
    flagSuccessRecord : boolean = false;
    termsLists        : DinamycData[];
    messageStatus     : string;
    messageResponse   : string;
    headerMessage     : string;
    codeResponse      : number  = 0;
    totalRecords      : number;

    constructor(private api: ApiService,
                private modalService: SuiModalService) { } 

    ngOnInit() {
        this.displayAllRecords();
    }

    ngOnDestroy(): void {
        // TODO: Implement unsubscribe
    }

    displayAllRecords(): void {
        // TODO: Implement observers
        this.api.getRequest("/api/admin/4/get/section")
                .subscribe(res => {
                    this.termsLists   = res['body'].data;
                    this.totalRecords = this.termsLists.length;
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Soft-delete the information selected by the user once clicks
     * the delete button
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    deleteRecord(title: string, message: string, id: number, button1: string, button2: string) : void {
        let modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService
            .open(modal)
            .onApprove(() => {
                this.api.deleteRequest(`/api/admin/${id}/delete`)
                    .subscribe((res: any) => {
                        // TODO: What code should be put here for applying observers?
                        if (res['body'].code == 200 && res['body'].status == 'OK') {
                            let data = res['body'].data;

                            this.flagSuccessRecord = true;
                            this.messageStatus     = res['body'].status;
                            this.messageResponse   = data.message;
                            this.codeResponse      = res['body'].code;
                            this.headerMessage     = data.header;

                            this.displayAllRecords();
                        }

                        if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                            this.flagDeleteRecord = true;
                            this.codeResponse     = res['body'].code;
                            this.messageStatus    = res['body'].status;
                            this.messageResponse  = res['body'].message;
                        }
                    },
                    (error: any) => {
                        // TODO: What code should be put here?
                    },
                    () => {
                        // TODO: What code should be put here?
                    });
            })
            .onDeny(() => {});
    }

    /**
     * Method for active or inactive records on the admin section
     * that will be displayed on the page for all the users
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param value number
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public open(title: string, message: string, id: number, value: number, button1: string, button2: string)
    {
        const modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService
            .open(modal)
            .onApprove(() => this.modalOptions(id, value))
            .onDeny(() => {});
    }

    /**
     * Method will be used to dismiss the message displayed in the
     * banner once the user executes an actions such as delete records
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message: IMessage) {
        this.flagDeleteRecord  = false;
        this.flagSuccessRecord = false;

        this.codeResponse    = 0;
        this.messageStatus   = '';
        this.messageResponse = '';
        this.headerMessage   = '';
    }

    /**
     * Method for make a request once we get the url
     * used for make the call to the api created
     * 
     * @param id number
     * @param value number
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private modalOptions(id: number, value: number): void
    {
        let urlString = this.stringRequest(id, value);

        this.api.getRequest(urlString)
                .subscribe((res:any) => {
                    // TODO: What code should be put here for applying observers?
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let responseData = res['body'].data;

                        this.flagSuccessRecord = true;
                        this.codeResponse      = res['body'].code;
                        this.messageStatus     = res['body'].status
                        this.messageResponse   = responseData.message;
                        this.headerMessage     = responseData.headerMsg;

                        this.displayAllRecords();
                    }

                    if (res['body'].status == 'ERROR' && res['body'].code == 400)
                    {
                        this.flagDeleteRecord = true;
                        this.messageStatus    = res['body'].status;
                        this.codeResponse     = res['body'].code;
                        this.messageResponse  = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Private method will be used for get the url will be
     * called the function once the modal has been approved
     * 
     * @param id number
     * @param value number
     * @return string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private stringRequest(id, value): string
    {
        let stringRequest;
        switch(value) {
            case 1: // Active
                stringRequest = `/api/availability/${id}/1/admin`;
                break;
            case 2: // Inactive
                stringRequest = `/api/availability/${id}/2/admin`;
                break;
        }

        return stringRequest;
    }
}
