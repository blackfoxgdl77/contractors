import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTermsConditionsComponent } from './admin-terms-conditions.component';

describe('AdminTermsConditionsComponent', () => {
  let component: AdminTermsConditionsComponent;
  let fixture: ComponentFixture<AdminTermsConditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTermsConditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTermsConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
