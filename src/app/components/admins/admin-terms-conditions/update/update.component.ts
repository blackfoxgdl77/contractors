import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { IMessage, SuiModalService } from 'ng2-semantic-ui';
import { DinamycData } from 'src/app/models/dinamyc-data';
import { ModalService } from 'src/app/services/modal.service';
import { EmbedVideoService } from 'ngx-embed-video';
import { AcceptRejectModal } from 'src/app/helpers/AcceptRejectModal';

@Component({
    selector: 'app-update',
    templateUrl: './update.component.html',
    styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
    public title  : string = "Terms and Conditions / Update Record";
    public breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '/admin/administrator', isLink: true },
        { text: 'Terms and Conditions - List', link: '/admin/termsconditions/list', isLink: true },
        { text: 'Terms and Conditions - Update', link: '', isLink: false }
    ];

    public formGroupE   : FormGroup;
    public editValues   : [];
    public paramId      : any;
    public submitedForm : boolean = false;
    public flagError    : boolean = false;
    public flagSuccess  : boolean = false;
  
    public messages    : string = '';
    public statusResp  : string = '';
    public codeResp    : number = 0;
    public errorMsg    : string = '';

    public fileString : string;
    public id         : number;
    public typeFile   : number;
    public nameString : string;
    public videoImage : string;

    public editorConfig = {
        editable: true,
        spellcheck: true,
        height: '15rem',
        minHeight: '5rem',
        placeholder: 'Type a description...',
        translate: 'yes'
    };
  
    constructor(private api: ApiService,
                private modal: ModalService,
                private modalService: SuiModalService,
                private videos: EmbedVideoService,
                private fb: FormBuilder, 
                private activatedRoute: ActivatedRoute,
                private _router : Router) { }
  
    ngOnInit() {
        this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
        this.api.getRequest(`/api/admin/${this.paramId}/get`)
                .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK') {
                        this.editValues = res.body['data'][0];
                        this.statusResp = res.body['status'];
                        this.codeResp   = res.body['code'];
                        this.errorMsg   = res.body['message'];
                        this.typeFile   = this.editValues['type_file'];
                        this.id         = this.editValues['id'];
                        this.fileString = (this.typeFile == 1) 
                                            ? this.editValues['file_size']
                                            : this.videos.embed(this.editValues['url'], { attr: { width: 800, height: 400}});
                        this.nameString = this.editValues['original_name'];
                        this.createSnapshotVideo(this.editValues);
    
                        if (parseInt(this.editValues['type_file']) == 1) {
                            this.formGroupE.controls.urlVideo.disable();
                        } else {
                            this.formGroupE.controls.image.disable();
                        }
    
                        this.fillDataValues();
                    }
    
                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR') {
                        this.statusResp = res.body['status'];
                        this.codeResp   = res.body['code'];
                        this.errorMsg   = res.body['message'];
                    }
                });
    
        this.validations();
    }
  
    /**
     * Method for validate if the fields are filled as expected
     * at the moment to edit records
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validations(): void {
        this.formGroupE = this.fb.group({
            section     : [''],
            description : ['', [Validators.required]],
            options     : ['', [Validators.required]],
            status      : ['', [Validators.required]],
            urlVideo    : [''],
            image       : ['']
        });
    
        this.formGroupE.controls['section'].disable();
    }

    /**
     * Method used to created the snapshot from the url given as parameter
     * in the function that was created to return the html will be displayed
     * 
     * @param url object
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    createSnapshotVideo(url: object): void {
        if (url['type_file'] == 2) {
            this.videos.embed_image(url['url'], { image: 'default' })
                       .then(res => {
                           this.videoImage = res.html;
                        });
        }
    }
  
    /**
     * Method will be used to assign variables and will be
     * used to display the data on the form before to update the
     * record selected by the user
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    fillDataValues(): void {
        if (this.statusResp === 'OK' && this.codeResp === 200) {
            this.formGroupE.patchValue({
            description : this.editValues['description'],
            section     : this.editValues['section'],
            urlVideo    : this.editValues['url'],
            options     : this.editValues['type_file'],
            status      : this.editValues['status']
            });
        } else {
            this.messages = this.statusResp + '. ' + this.errorMsg;
        }
    }
  
    /**
     * Method to update an record that exists in the database, so won't
     * create a new records
     * 
     * @author Ruen Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    updateRecord(): void {
        let values = this.formGroupE.value;
        Object.assign(values, { section : 'Terms and Conditions' });
        this.submitedForm = true;

        if (values['options'] == '1') {
            Object.assign(values, { urlVideo : '' });
        }
    
        if (this.formGroupE.invalid) {
            this.flagError = (this.flagError) ? this.flagError : !this.flagError;
    
            return ;
        }
    
        this.flagError   = false;
        this.api.updateRequest(`/api/admin/${this.paramId}/update`, JSON.stringify(values))
                .subscribe(res => {
                    if (res.body['code'] == 200 && res.body['status'] == 'OK') {
                        let dataReturned = res.body['data'];
                        this.codeResp    = res.body['code'];
                        this.statusResp  = res.body['status'];
                        this.errorMsg    = dataReturned['message'];
                        this.flagSuccess = true;
                    }
    
                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR') {
                        this.flagError  = true;
                        this.codeResp   = res.body['code'];
                        this.statusResp = res.body['status'];
                        this.errorMsg   = res.body['message'];
                    }
                });
    }

    /**
     * Method used for delete an image when the user click the link
     * next to the image
     * 
     * @param id number
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    deleteImage(title: string, message: string, id: number, button1: string, button2: string): void {
        console.log(title, message, id, button1, button2);
        const modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService.open(modal)
                  .onApprove(() => {
                    this.api.deleteRequest(`/api/delete/${id}/files`)
                            .subscribe((res: any) => {
                                if (res.body['code'] == 200 && res.body['status'] == 'OK') {
                                    let returnedData = res.body['data'];
            
                                    this.flagSuccess = true;
                                    this.codeResp    = res.body['code'];
                                    this.statusResp  = res.body['status'];
                                    this.errorMsg    = returnedData['message'];
                                }
            
                                if (res.body['code'] == 400 && res.body['status'] == 'ERROR') {
                                    this.codeResp   = res.body['code'];
                                    this.statusResp = res.body['status'];
                                    this.errorMsg   = res.body['message'];
                                    this.flagError  = true;
                                }
                            },
                            (error: any) => {
                                // What code should be put here?
                            },
                            () => {
                                // What code should be put here?
                            });
                  })
                  .onDeny(() => {})
    }
  
    /**
     * Method will cancel the actions and the user
     * will be redirected to the main view, where the users
     * will see the list of all the records
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    cancelOptions() {
        return this._router.navigate(['/admin/termsconditions/list']);
    }
  
    /**
     * Method will be used to disable or enable the fields
     * depending on the option selected on edit view
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    disabledEnabledFields(event: any) {
        if (event.target.value == '1') {
            this.formGroupE.controls['image'].enable();
            this.formGroupE.controls['urlVideo'].disable();
            this.formGroupE.controls.urlVideo.setValue('');
        } else {
            this.formGroupE.controls['image'].disable();
            this.formGroupE.controls['urlVideo'].enable();
            this.formGroupE.controls.image.setValue('');
        }
    }
  
    /**
     * Method will return the object could we use
     * for handling the form object in the template
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    get f() {
        return this.formGroupE.controls;
    }
  
    /**
     * Method will close the modal with sucess message once
     * the user has updated a record
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    dismiss(message : IMessage) {
        this.flagSuccess = false;
    }

    /**
     * Method for view file once the user click the button view image
     * in edit section
     * 
     * @param id string
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    viewerFilesOpen(id: string): void {
        this.modal.open(id);
    }

    /**
     * Method for close the file once the user clicks on the close button
     * in edit section
     * 
     * @param id string
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    viewerFilesClose(id: string): void {
        this.modal.close(id);
    }
}
