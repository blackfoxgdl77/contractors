import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHowItWorksComponent } from './admin-how-it-works.component';

describe('AdminHowItWorksComponent', () => {
  let component: AdminHowItWorksComponent;
  let fixture: ComponentFixture<AdminHowItWorksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHowItWorksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHowItWorksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
