import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Users } from 'src/app/models/users';
import { SuiModal, SuiModalService } from 'ng2-semantic-ui';
import { AcceptRejectModal } from 'src/app/helpers/AcceptRejectModal';
import { IMessage } from 'ng2-semantic-ui';
import { HttpResponse } from '@angular/common/http';

@Component({
    selector: 'app-list-all-users',
    templateUrl: './list-all-users.component.html',
    styleUrls: ['./list-all-users.component.scss']
})
export class ListAllUsersComponent implements OnInit {
    title  : string = "List of Users / List Users";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '/admin/administrator', isLink: true },
        { text: 'List of Users - List', link: '', isLink: false }
    ];

    usersList    : Users[];
    totalRecords : number;
    statusData   : string  = '';
    codeData     : number  = 0;
    headerData   : string  = '';
    messageData  : string  = '';
    flagUpdate   : boolean = false;

    constructor(public api: ApiService,
                public modalService: SuiModalService) { }

    ngOnInit() {
        this.api.getRequest(`/api/users/all`)
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    localStorage.setItem('userT2', res.headers.get('_token_log'));
                    localStorage.setItem('userT1', res.headers.get('_token_sec'));
                    localStorage.setItem('number', res.headers.get('_token_user'));
                    
                    this.usersList    = res['body'].data;
                    this.totalRecords = (this.usersList) ? this.usersList.length : 0;
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method for enabled or disabled the record of the user
     * depending on the action selected by the user
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @param user_id integer
     * @param user_status integer
     * @version 1.0
     * @platform contractorsbids
     */
    updateStatus(user_id, user_status) {
        let data = {'user_id': user_id, 'user_status': user_status};

        this.api.postRequest(`/api/users/users/update`, JSON.stringify(data))
                .subscribe((res: any) => {
                    // TODO: Implementing Observables
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let returnedData = res['body'].data;
                        this.flagUpdate  = true;                       
                        this.codeData    = res['body'].code;
                        this.statusData  = res['body'].status;
                        this.headerData  = returnedData.header;
                        this.messageData = returnedData.message;
                    }

                    if (res['body'].code == 200 && res['body'].status == 'ERROR') {
                        this.flagUpdate = false;
                        this.codeData    = res['body'].code;
                        this.statusData  = res['body'].status;
                        this.messageData = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method to open the modal and give the user the option
     * to execute depending the messages displayed on the modal
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param value number
     * @param options string
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public open(title: string, message: string, id: number, value: number, button1: string, button2: string)
    {
        const modal = new AcceptRejectModal(title, message, button1, button2);
        
        this.modalService
            .open(modal)
            .onApprove(() => this.modalOptions(id, value))
            .onDeny(() => {});
    }

    /**
     * Soft-delete the information selected by the user once clicks
     * the delete button
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    deleteRecord(title: string, message: string, id: number, button1: string, button2: string) : void {
        let modal = new AcceptRejectModal(title, message, button1, button2);

        this.modalService
            .open(modal)
            .onApprove(() => {
                this.api.deleteRequest(`/api/users/${id}/delete`)
                    .subscribe((res: any) => {
                        // TODO: Implementing Observables
                        if (res['body'].code == 200 && res['body'].status == 'OK') {
                            let data = res['body'].data;

                            this.flagUpdate  = true;
                            this.statusData  = res['body'].status;
                            this.messageData = data.message;
                            this.codeData    = res['body'].code;
                            this.headerData  = data.header;
                        }

                        if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                            this.flagUpdate  = true;
                            this.codeData    = res['body'].code;
                            this.statusData  = res['body'].status;
                            this.messageData = res['body'].message;
                        }
                    },
                    (error: any) => {
                        // TODO: What code should put here?
                    },
                    () => {
                        // TODO: What code should be put here?
                    });
            })
            .onDeny(() => {});
    }

    /**
     * Method for make a request once we get the url
     * used for make the call to the api created
     * 
     * @param id number
     * @param value number
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    modalOptions(id: number, value: number)
    {
        let urlString = this.stringRequest(id, value);

        this.api.getRequest(urlString)
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let dataReturned = res['body'].data;

                        this.codeData    = res['body'].code;
                        this.statusData  = res['body'].status;
                        this.flagUpdate  = true;
                        this.headerData  = dataReturned.header;
                        this.messageData = dataReturned.message;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.flagUpdate  = false;
                        this.codeData    = res['body'].code;
                        this.statusData  = res['body'].status;
                        this.messageData = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Private method will be used for get the url will be
     * called the function once the modal has been approved
     * 
     * @param id number
     * @param value number
     * @return string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private stringRequest(id, value): string
    {
        let stringRequest;
        switch(value) {
            case 1: // Active
                stringRequest = `/api/availability/${id}/0/users`;
                break;
            case 2: // Inactive
                stringRequest = `/api/availability/${id}/1/users`;
                break;
            case 3:
                stringRequest = `/api/warning/${id}/admin`;
                break;
        }

        return stringRequest;
    }

    /**
     * Method will be disabled once the user clicks on the cross
     * for clear the messages
     * 
     * @param message IMessage
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorbids
     */
    public dismiss(message: IMessage)
    {
        this.headerData = '';
        this.flagUpdate = false;
        this.codeData   = 0;
        this.statusData = '';
        this.messageData = '';
    }
}
