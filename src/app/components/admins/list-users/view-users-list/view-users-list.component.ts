import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { IMessage } from 'ng2-semantic-ui';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SuiModalService } from 'ng2-semantic-ui';
import { AcceptRejectModal } from 'src/app/helpers/AcceptRejectModal';

@Component({
    selector: 'app-view-users-list',
    templateUrl: './view-users-list.component.html',
    styleUrls: ['./view-users-list.component.scss']
})
export class ViewUsersListComponent implements OnInit {
    title  : string = "List of Users / View User Information";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Administrator', link: '/admin/administrator', isLink: true },
        { text: 'List Of Users - List', link: '/admin/users-lists/list-all-users', isLink: true },
        { text: 'List Of Users - View', link: '', isLink: false }
    ];

    adminPersonal : FormGroup;
    paramId       : any;
    typeStatus    : any;
    flag          : boolean = false;
    code          : number = 0;
    status        : string = '';
    messages      : string = '';
    userInfo      : any = {};
    id            : number;
    is_active     : number;
    header        : string = '';
    username      : string = '';

    constructor(private api: ApiService,
                private fb: FormBuilder,
                private modalService: SuiModalService,
                private activated: ActivatedRoute) { }

    ngOnInit() {
        this.paramId    = this.activated.snapshot.paramMap.get('id');
        this.typeStatus = this.activated.snapshot.paramMap.get('type');

        this.api.getRequest(`/api/users/${this.paramId}/info`)
                .subscribe((res: any) => {
                    // TODO: What code should be put here?
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        this.userInfo  = res['body'].data;
                        this.code      = res['body'].code;
                        this.status    = res['body'].status;
                        this.is_active = this.userInfo.is_active;
                        this.id        = this.userInfo.id;
                        this.username  = this.userInfo.username;

                        this.fillValidations();
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.flag     = false;
                        this.code     = res['body'].code;
                        this.status   = res['body'].status;
                        this.messages = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });

        this.validationsFields();
    }

    /**
     * Validate the fields, in this case should be disabled and with no
     * rules about the fields in the form, this will work due to is a 
     * function for just display the values to the admin user
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validationsFields(): void {
        this.adminPersonal = this.fb.group({
            username:    [{ value: '', disabled: true }],
            name:        [{ value: '', disabled: true }],
            email:       [{ value: '', disabled: true }],
            phone:       [{ value: '', disabled: true }],
            payments:    [{ value: '', disabled: true }],
            bbb_account: [{ value: '', disabled: true }],
            accredited:  [{ value: '', disabled: true }],
            raiting:     [{ value: '', disabled: true }],
            wcb_number:  [{ value: '', disabled: true }],
            address:     [{ value: '', disabled: true }]
        });
    }

    /**
     * Method will fill all the fields in the view of the users
     * in admin section, where the users could enabled or disabled
     * a specific user
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    fillValidations(): void {
        this.adminPersonal.patchValue({
            username    : this.userInfo['username'],
            name        : this.userInfo['name'],
            email       : this.userInfo['email'],
            phone       : this.userInfo['phone'],
            payments    : this.userInfo['payment'],
            bbb_account : this.userInfo['bbb_account'],
            accredited  : this.userInfo['accredited'],
            raiting     : this.userInfo['raiting'],
            wcb_number  : this.userInfo['wcb_number'],
            address     : this.userInfo['address']
        });
    }

    /**
     * Method to open the modal and give the user the option
     * to execute depending the messages displayed on the modal
     * 
     * @param title string
     * @param message string
     * @param id number
     * @param value number
     * @param options string
     * @param button1 string
     * @param button2 string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public open(title: string, message: string, id: number, value: number, button1: string, button2: string)
    {
        const modal = new AcceptRejectModal(title, message, button1, button2);
        
        this.modalService
            .open(modal)
            .onApprove(() => this.modalOptions(id, value))
            .onDeny(() => {});
    }

    /**
     * Method for make a request once we get the url
     * used for make the call to the api created
     * 
     * @param id number
     * @param value number
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    modalOptions(id: number, value: number)
    {
        let urlString = this.stringRequest(id, value);

        this.api.getRequest(urlString)
                .subscribe((res: any) => {
                    // TODO: Implementing Observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let dataReturned = res['body'].data;

                        this.code     = res['body'].code;
                        this.status   = res['body'].status;
                        this.flag     = true;
                        this.header   = dataReturned.header;
                        this.messages = dataReturned.message;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.flag     = false;
                        this.code     = res['body'].code;
                        this.status   = res['body'].status;
                        this.messages = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Private method will be used for get the url will be
     * called the function once the modal has been approved
     * 
     * @param id number
     * @param value number
     * @return string
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private stringRequest(id, value): string
    {
        let stringRequest;
        switch(value) {
            case 1: // Active
                stringRequest = `/api/availability/${id}/0/users`;
                break;
            case 2: // Inactive
                stringRequest = `/api/availability/${id}/1/users`;
                break;
            case 3:
                stringRequest = `/api/warning/${id}/admin`;
                break;
        }

        return stringRequest;
    }

    /**
     * Method will be disabled once the user clicks on the cross
     * for clear the messages
     * 
     * @param message IMessage
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorbids
     */
    public dismiss(message: IMessage)
    {
        this.header   = '';
        this.flag     = false;
        this.code     = 0;
        this.status   = '';
        this.messages = '';
    }
}
