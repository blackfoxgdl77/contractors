import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesPagesComponent } from './messages-pages.component';

describe('MessagesPagesComponent', () => {
  let component: MessagesPagesComponent;
  let fixture: ComponentFixture<MessagesPagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesPagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
