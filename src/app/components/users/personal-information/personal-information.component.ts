import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { IMessage } from 'ng2-semantic-ui';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-personal-information',
    templateUrl: './personal-information.component.html',
    styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit, OnDestroy {
    personalForm : FormGroup;
    submitedForm : boolean = false;
    errorFlag    : boolean = false;
    successFlag  : boolean = false;
    paramId      : any;
    usersInfo    : any = {};
    statusCode   : string;
    message      : string;
    errorMsg     : string;
    numberCode   : number;
    flagAction   : string;

    constructor(private api: ApiService, private fb: FormBuilder, private activateRoute: ActivatedRoute) { }

    ngOnInit() {
        this.paramId = this.activateRoute.snapshot.paramMap.get('id');
        this.validations();

        this.getPersonalInformation();
    }

    ngOnDestroy(): void {
        // TODO: Implement unsubscribe
    }

    /**
     * 
     */
    getPersonalInformation(): void {
        this.api.getRequest(`/api/users/${this.paramId}/info`)
                .subscribe((res: any) => {
                    // TODO: Implement code for observers
                    this.usersInfo  = res['body'].data;
                    this.statusCode = res['body'].status;
                    this.numberCode = res['body'].code;
                    this.errorMsg   = (res['body'].code == 400) ? res['body'].message : '';

                    this.fillValidation();
                },
                (error: any) => {
                    // TODO: What code should be implemented?
                },
                () => {
                    // TODO: What code should be implemented?
                });
    }

    /**
     * Method will be used to validate the form and the fileds
     * marked as required
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validations(): void {
        this.personalForm = this.fb.group({
            username:    ['', [Validators.required]],
            name:        ['', [Validators.required]],
            email:       ['', [Validators.required, Validators.email]],
            phone:       ['', [Validators.required]],
            payments:    [''],
            bbb_account: [''],
            accredited:  [''],
            raiting:     [''],
            wcb_number:  [''],
            address:     ['']
        });

        this.personalForm.controls['username'].disable();
    }

    /**
     * Method will be used to fill the form at the
     * moment to go to the update users information
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    fillValidation(): void {
        if (this.numberCode == 200) {
            this.personalForm.patchValue({
                username    : this.usersInfo['username'],
                name        : this.usersInfo['name'],
                email       : this.usersInfo['email'],
                phone       : this.usersInfo['phone'],
                payments    : this.usersInfo['payment'],
                bbb_account : this.usersInfo['bbb_account'],
                accredited  : this.usersInfo['accredited'],
                raiting     : this.usersInfo['raiting'],
                wcb_number  : this.usersInfo['wcb_number'],
                address     : this.usersInfo['address']
            });
        } else {
            this.message = this.errorMsg;
        }
    }

    /**
     * Method for get the information of the form and send
     * the data to the server for execute the action required
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    updatePersonalInformation(): void {
        this.submitedForm = true;
        let values = this.personalForm.value;

        if (this.personalForm.invalid) {
            this.errorFlag = (this.errorFlag) ? this.errorFlag : !this.errorFlag;
            return ;
        }

        this.errorFlag   = false;
        this.successFlag = true;

        this.api.updateRequest(`/api/users/info/${this.paramId}/update`, JSON.stringify(values))
                .subscribe((res: any) => {
                    // TODO: Implement code for observers
                    this.statusCode = res['body'].status;
                    this.numberCode = res['body'].code;
                    this.message    = res['body'].data.message;
                    this.flagAction = "update";
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }
}
