import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { IMessage } from 'ng2-semantic-ui';
import { CustomValidators } from 'src/app/services/custom-validators';

@Component({
  selector: 'app-change-pwd',
  templateUrl: './change-pwd.component.html',
  styleUrls: ['./change-pwd.component.scss']
})
export class ChangePwdComponent implements OnInit {
    changeForm    : FormGroup;
    loading       : boolean;
    errorFlag     : boolean = false;
    successFlag   : boolean = false;
    submittedForm : boolean = false;
    code          : number  = 0;
    isValid       : number  = 0;
    status        : string  = '';
    messages      : string  = '';
    personalMsg   : string  = '';
    paramToken    : string  = '';
    paramId       : any;

    title  : string = "Change Password";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Change Password', link: '', isLink: false }
    ];

    constructor(private apiService: ApiService,
                private activatedRouter: ActivatedRoute,
                private changeBuilder: FormBuilder) { }

    ngOnInit() {
        this.paramToken = this.activatedRouter.snapshot.paramMap.get('reset_token');
        this.paramId    = this.activatedRouter.snapshot.paramMap.get('reset_id');

        this.apiService.getRequest(`/api/users/valid/${this.paramId}/change`)
                       .subscribe((res: any) => {
                           // TODO: implement observables
                           if (res['body'].code == 200 && res['body'].status == 'OK')
                           {
                               let returnedData = res['body'].data;

                               this.isValid     = returnedData.valid;
                               this.personalMsg = returnedData.message;
                           }

                           if (res['body'].code == 400 && res['body'].status == 'ERROR')
                           {
                               this.errorFlag = true;
                               this.code      = res['body'].code;
                               this.status    = res['body'].status;
                               this.messages  = res['body'].message;
                           }
                       },
                       (error: any) => {
                           // TODO: What code should be put here?
                       },
                       () => {
                           // TODO: What code should be put here?
                       });

        this.changePwdValidations();
    }

    /**
     * Method will be used to get the validations
     * is going to be applied in the form to know if
     * must display the errors or not
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    changePwdValidations(): void {
        this.changeForm = this.changeBuilder.group({
            password  : ['', [Validators.required, Validators.minLength(6)]],
            cpassword : ['', [Validators.required, Validators.minLength(6)]]
        },{
            // check whether our password and confirm password match
            validator: CustomValidators.passwordMatchValidator
        });
    }

    /**
     * Method is going to make the request to the
     * service and catch the reply of the service
     * to display an especific message depending on the
     * response of the service
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    changePwd(): any {
        this.submittedForm = true;
        let values = this.changeForm.value;
        Object.assign(values, { reset_token: this.paramToken });
        Object.assign(values, { reset_id: this.paramId });

        if (this.changeForm.invalid) {
            this.errorFlag = (this.errorFlag) ? this.errorFlag : !this.errorFlag;
            return ;
        }

        this.errorFlag = false;
        this.successFlag = true;

        this.apiService.postRequest(`/api/user/update/password`, JSON.stringify(values))
                       .subscribe((res: any) => {
                           // TODO: Implement observers
                           if (res['body'].code == 200 && res['body'].status == 'OK')
                           {
                                let returnedData = res['body'].data;
                                   
                                this.successFlag = true;
                                this.code        = res['body'].code;
                                this.status      = res['body'].status;
                                this.messages    = returnedData.message;

                                this.changeForm.reset();
                                this.submittedForm = false;
                           }

                           if (res['body'].code == 400 && res['body'].status == 'ERROR')
                           {
                               this.errorFlag = true;
                               this.code      = res['body'].code;
                               this.status    = res['body'].status;
                               this.messages  = res['body'].message;
                           }
                       },
                       (error: any) => {
                           // TODO: What code should be put here?
                       },
                       () => {
                           // TODO: What code should be put here?
                       });
    }

    /**
     * Method will clear the fields from the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearField(): void {
        this.errorFlag     = false;
        this.successFlag   = false;
        this.submittedForm = false;

        this.changeForm.reset();
    }

    /**
     * Method to get the elements of the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    get f() {
        return this.changeForm.controls;
    }

    /**
     * Method will clean all the fields have been
     * set by the users and also removes the banners displayed
     * with the messages
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message: IMessage) {
        this.successFlag   = false;
        this.submittedForm = false;;

        this.changeForm.reset();
        this.code     = 0;
        this.messages = '';
        this.status   = '';
    }
}
