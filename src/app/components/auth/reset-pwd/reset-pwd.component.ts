import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { IMessage } from 'ng2-semantic-ui';
import { BreadcrumbsComponent } from 'src/app/components/breadcrumbs/breadcrumbs.component';
import { ApiService } from 'src/app/services/api.service';
import { CustomValidators } from 'src/app/services/custom-validators';

@Component({
  selector: 'app-reset-pwd',
  templateUrl: './reset-pwd.component.html',
  styleUrls: ['./reset-pwd.component.scss']
})
export class ResetPwdComponent implements OnInit {
    resetPwdForm  : FormGroup;
    errorFlag     : boolean = false;
    successFlag   : boolean = false;
    submittedForm : boolean = false;
    statusCode    : number  = 0;
    statusMessage : string  = '';
    statusResp    : string  = '';

    title  : string   = "Reset Password";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Reset Password', link: '', isLink: false }
    ];

    steps : Object[] = [
        { title: 'New Password', icons: 'lock', description: 'Reset your Password' },
        { title: 'Receive Email', icons: 'envelope', description: 'Receive Email with the Instructions' },
        { title: 'Start to Post Projects', icons: 'pencil alternate', description: 'Start to Post your Projects' }
    ];

    constructor(private apiServices: ApiService,
                private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.resetPwdValidations();
    }

    /**
     * Validate the field required in the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    resetPwdValidations(): void {
        this.resetPwdForm = this.formBuilder.group({
            email: ['', [Validators.email, Validators.required]],
            confirm_email: ['', [Validators.email, Validators.required]],
        },
        {
            // validate match emails
            validator : CustomValidators.emailMatchValidator
        });
    }

    /**
     * Method will call the api to reset the password set by the user
     * once has been changed
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    resetPwd() {
        this.submittedForm = true;
        let values = this.resetPwdForm.value;
        
        if (this.resetPwdForm.invalid) {
            this.errorFlag = (this.errorFlag) ? this.errorFlag : !this.errorFlag;
            return ;
        }

        this.errorFlag   = false;
        this.successFlag = true;

        this.apiServices.postRequest('/api/users/reset/pwd', JSON.stringify(values))
                        .subscribe((res: any) => {
                            // TODO: Implement observables
                            if (res['body'].code == 200 && res['body'].status == 'OK') {
                                let returnedData = res['body'].data;
                                
                                this.statusCode    = res['body'].code;
                                this.statusMessage = returnedData.message;
                                this.statusResp    = res['body'].status;

                                this.clearPwdFields();
                            }

                            if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                                this.statusCode    = res['body'].code;
                                this.statusMessage = res['body'].message;
                                this.statusResp    = res['body'].status;
                            }
                        },
                        (error: any) => {
                            // TODO: What code should be put here?
                        },
                        () => {
                            // TODO: What code should be put here?
                        });
    }

    /**
     * Method will be used to clear the fields in the reset password form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearPwdFields(): void {
        this.submittedForm = false;
        this.errorFlag     = false;

        this.resetPwdForm.reset();
    }

    /**
     * Check if the flag is set to true
     * for displating success or error messages
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @params typeError string
     */
    isMessageDisplayed(typeError: string): boolean {
        if (typeError == 'error') {
            return this.errorFlag;
        }
    }

    /**
     * Method will be used to dismiss a success message,
     * due to will appear once the process of send the
     * form has been done successfully with no error
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @params void
     */
    dismissMessage(message: IMessage): void {
        message.dismiss();
    }

    /**
     * Method to get the elements of the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    get f() {
        return this.resetPwdForm.controls;
    }

    /**
     * Dismiss the button once the user clicks in the close
     * icon and error
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message: IMessage) {
        this.clearPwdFields();
        
        this.statusCode    = 0;
        this.statusResp    = '';
        this.statusMessage = '';
        this.errorFlag     = false;
        this.successFlag   = false;
    }

}
