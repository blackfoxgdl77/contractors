import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { IMessage } from 'ng2-semantic-ui';
import { BreadcrumbsComponent } from 'src/app/components/breadcrumbs/breadcrumbs.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    formLogin     : FormGroup;
    loading       : boolean = false;
    credentials   : Object[] = [];
    errorFlag     : boolean = false;
    successFlag   : boolean = false;
    submittedForm : boolean = false;
    statusMessage : string;
    statusResp    : string;
    statusCode    : number;  
    user          : string = "";      

    title  : string = "Sign In!";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Sign In!', link: '', isLink: false }
    ];

    constructor(private apiServices: ApiService,
                private authService: AuthService,
                private loginBuilder: FormBuilder,
                private router: Router) { }

    ngOnInit() {
        this.loginValidations();
    }

    /**
     * Method will use to check the fields of the forms
     * and the validations will be applied depending on
     * the login validations
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    loginValidations(): void {
        this.formLogin = this.loginBuilder.group({
            user: ['', [Validators.required]],
            pwd: ['', [Validators.required]],
        });
    }
    
    /**
     * Funtions will change every word in upper case, this is required
     * for the login
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    onUpperFunction(event: any) {
        this.user = event.target.value.toUpperCase();
    }

    /**
     * Method will be used for validate loginn once the
     * user wants to load the data as expected
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    login() {
        let values = this.formLogin.value;
        this.submittedForm = true;

        if (this.formLogin.invalid) {
            this.errorFlag = (this.errorFlag) ? this.errorFlag : !this.errorFlag;
            return ;
        }
        
        this.clearMessagesForms();
        this.apiServices.postRequest('/api/auth/login', JSON.stringify(values))
                        .subscribe((res: any) => {
                            // TODO: Should be implemented observables
                            if (res['body'].code == 200 && res['body'].status == 'OK') 
                            {
                                let userdata = res['body'].data;
                                console.log(userdata.token);
                               
                                localStorage.setItem('access_token', userdata.token.create_token);
                                localStorage.setItem('refresh_token', userdata.token.refresh_token);
                                localStorage.setItem('username', userdata.user);
                                localStorage.setItem('userT1', userdata.s_token.t1);
                                localStorage.setItem('userT2', userdata.s_token.t2);
                                //localStorage.setItem('number', userdata['number']);
                                localStorage.setItem('isAdmin', userdata.admin);
                                
                                this.loginHistory();
                                //this.loginSuccess();
                            }

                            if (res['body'].code == 405 && res['body'].status == 'ERROR')
                            {
                                this.statusCode    = res['body'].code;
                                this.statusMessage = res['body'].message;
                                this.statusResp    = res['body'].status;
                            }

                            if (res['body'].code == 400 && res['body'].status == 'ERROR') 
                            {
                                this.statusCode    = res['body'].code;
                                this.statusMessage = res['body'].message;
                                this.statusResp    = res['body'].status;
                            }
                        },
                        (error: any) => {
                            // TODO: What code should be put here?
                        },
                        () => {
                            // TODO: What code should be put here?
                        });
    }

    /**
     * Method to update the value of the login history
     * to set the new values and receive another value to
     * store and use after it
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    loginHistory(): void {
        let send_data = { 'token': localStorage.getItem('access_token'), 
                          'userS': localStorage.getItem('userT1'),
                          'userL': localStorage.getItem('userT2') };

        this.apiServices.postRequest(`/api/users/login/1/history`, JSON.stringify(send_data))
                        .subscribe((res: any) => {
                            // TODO: What code should be put here?
                            if (res['body'].code == 200 && res['body'].status == 'OK')
                            {
                                localStorage.setItem('login_history', res['body'].history);

                                this.loginSuccess();
                            }

                            if (res['body'].code == 400 && res['body'].status == 'ERROR')
                            {
                                this.statusCode    = res['body'].code;
                                this.statusMessage = res['body'].message;
                                this.statusResp    = res['body'].status;
                            }
                        },
                        (error: any) => {
                            // TODO: What code should be put here?
                        },
                        () => {
                            // TODO: What code should be put here?
                        });
    }

    /**
     * Method will be used to redirect once the user has logged in
     * successfully
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids.com
     */
    loginSuccess(): void {
        let objectSend = { 'history' : localStorage.getItem('login_history'),
                           'userS'   : localStorage.getItem('userT1'),
                           'userL'   : localStorage.getItem('userT2') };
        
        this.apiServices.postRequest(`/api/users/update/information`, JSON.stringify(objectSend))
                        .subscribe((res: any) => {
                            // TODO: What code should be put here?
                            if (res['body'].code == 200 && res['body'].status == 'OK') 
                            {
                                localStorage.setItem('number', res['body'].number);

                                this.redirect();
                            }

                            if (res['body'].code == 400 && res['body'].status == 'ERROR')
                            {
                                this.statusCode    = res['body'].code;
                                this.statusResp    = res['body'].status;
                                this.statusMessage = res['body'].message;
                            }
                        },
                        (error: any) => {
                            // TODO: What code shoule be put here?
                        },
                        () => {
                            // TODO: What code should be put here?
                        });
    }

    /**
     * Method will be used to redirect once the user
     * has been logged in
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    redirect() {
        //this.router.navigate(['/']);
        window.location.href = '/';
    }

    /**
     * Private method for clear all the fields in the login
     * and removes the error messages banners as well
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private clearMessagesForms() : void {
        this.statusCode  = 0;
        this.statusResp  = '';
        this.errorFlag   = false;
        this.successFlag = true;
    }

    /**
     * Method will be used to validate if the user has logout
     * from the methods and will be able to close the session
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    logout(): void {
        let logout = { 'userS' : this.authService.userT1,
                       'userL' : this.authService.userT2 };

        this.authService.logout();
        this.apiServices.postRequest(`/api/users/logout`, JSON.stringify(logout))
                        .subscribe((res: any) => {
                            // TODO: apply the observers
                            window.location.href = '/';
                        },
                        (error: any) => {
                            // TODO: What code should be put here?
                        },
                        () => {
                            // TODO: What code should be put here?
                        });
    }

    /**
     * Method will clear the fields from the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearLoginFields(): void {
        this.errorFlag     = false;
        this.successFlag   = false;
        this.statusCode    = 0;
        this.statusResp    = '';
        this.submittedForm = false;

        this.formLogin.reset();
    }

    /**
     * Check if the flag is set to true
     * for displating success or error messages
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @params typeError string
     */
    isMessageDisplayed(typeError: string): boolean {
        if (typeError == 'error') {
            return this.errorFlag;
        }
    }

    /**
     * Method to get the elements of the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    get f() {
        return this.formLogin.controls;
    }
}
