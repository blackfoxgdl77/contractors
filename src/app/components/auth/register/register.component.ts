import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { ApiService } from 'src/app/services/api.service';
import { EncryptAuthService } from 'src/app/services/encrypt-auth.service';
import { ModalService } from 'src/app/services/modal.service';
import { BreadcrumbsComponent } from 'src/app/components/breadcrumbs/breadcrumbs.component';
import { IMessage } from 'ng2-semantic-ui';
import 'jquery';
import { Router } from '@angular/router';
import { CustomValidators } from 'src/app/services/custom-validators';

const ENCRYPT_KEY: string = 'tH1s!5THe@U73NTi(@tI0NC0d3#';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    contractorForm    : FormGroup;
    homeOwnerForm     : FormGroup;
    displayContractor : boolean;
    displayHomeOwner  : boolean;
    termsConditions   : string;
    errorFlagC        : boolean = false;
    errorFlagH        : boolean = false;
    submittedFormC    : boolean = false;
    submittedFormH    : boolean = false;
    successFlagC      : boolean = false;
    successFlagH      : boolean = false;
    errorStatus       : string;
    errorMessage      : string;
    errorCode         : number;

    title  : string = "Sign Up!";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'Sign Up!', link: '', isLink: false }
    ];

    steps : Object[] = [
        { title: 'New Account', icons: 'address card', description: 'Create New Account' },
        { title: 'Confirmation', icons: 'check circle', description: 'Confirm Your Account' },
        { title: 'Publish Projects', icons: 'pencil alternate', description: 'Start to Publish Your Projects' }
    ];

    constructor(private apiService: ApiService,
                private encryptService: EncryptAuthService,
                private modalService: ModalService,
                private contractorBuilder: FormBuilder,
                private homeownerBuilder: FormBuilder,
                private router: Router) {
    }

    ngOnInit() {
        (<any>$(".menu .item")).tab();
        this.validationsHomeForm();
        this.validationsContractorForm();

        this.apiService.getRequest(`/api/website/termsConditions`)
                       .subscribe((res: any) => {
                            let dataReturned = res['body'].data;

                            if (res['body'].code == 200 && res['body'].status == 'OK') {
                                this.termsConditions = dataReturned[0].description;
                            }

                            if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                                this.errorCode    = res['body'].code;
                                this.errorStatus  = res['bddy'].status;
                                this.errorMessage = res['body'].message;
                            }
                       },
                       (error: any) => {
                           // What code should be put here?   
                       },
                       () => {
                           // What code should be put here?
                       });
    }

    /**
     * Method for get the validations of the fields
     * depending on the form selected by the user wants
     * to create a new account
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validationsContractorForm(): void {
        this.contractorForm = this.contractorBuilder.group({
            name:             ['', [Validators.required, Validators.minLength(5)]],
            phone:            ['', [Validators.required]],
            email:            ['', [Validators.email, Validators.required]],
            accredited:       ['', [Validators.required]],
            wcb:              [''],
            password:         ['', [Validators.required, Validators.minLength(6)]],
            cpassword:        ['', [Validators.required, Validators.minLength(6)]],
            cTermsContractor: ['', [Validators.required]]
        },
        {
            // check whether our password and confirm password match
            validator: CustomValidators.passwordMatchValidator
        });
    }

    /**
     * Method for get the validations of the fields
     * depending on the form selected by the user wants
     * to create a new account
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    validationsHomeForm(): void {
        this.homeOwnerForm = this.homeownerBuilder.group({
            name:       ['', [Validators.required, Validators.minLength(3)]],
            phone:      ['', [Validators.required]],
            email:      ['', [Validators.required, Validators.email]],
            password:   ['', [Validators.required, Validators.minLength(6)]],
            cpassword:  ['', [Validators.required, Validators.minLength(6)]],
            cTermsHome: ['', [Validators.required]]
        },
        {
            // check whether our password and confirm password match
            validator : CustomValidators.passwordMatchValidator
        });
    }

    /**
     * Send the request to the service for create a new
     * account related to the contractor account and can
     * be created once the required fields have been filled up
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    newContractorAccount() {
        this.submittedFormC = true;
        let values = this.contractorForm.value;
        
        if (this.contractorForm.invalid) {
            this.errorFlagC = (this.errorFlagC) ? this.errorFlagC : !this.errorFlagC;
            return ;
        }

        this.setFlagsButton(1);
        values['type_user'] = 0;
        values['is_company'] = 1;
        
        this.apiService.postRequest('/api/auth/create', JSON.stringify(values))
                       .subscribe((res: any) => {
                           // TODO: Implement observables
                           if (res['body'].status == 'OK' && res['body'].code == 200) {
                                let registerData = res['body'].data;
                                this.redirectUser(registerData.user, registerData.id_user)
                           }
                           
                           if (res['body'].status == 'ERROR' && res['body'].code == 400) {
                                this.errorMessage = res['body'].message;
                                this.errorStatus  = res['body'].status;
                                this.errorCode    = res['body'].code;
                           }
                       },
                       (error: any) => {
                           // TODO: What code should be put here?
                       },
                       () => {
                           // TODO: What code should be put here?
                       }); 
    }

    /**
     * Send the request to the service for create a new
     * account related to the contractor account and can
     * be created once the required fields have been filled up
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    newHomeOwnerAccount() {
        this.submittedFormH = true;
        let values = this.homeOwnerForm.value;
        
        if (this.homeOwnerForm.invalid) {
            this.errorFlagH = (this.errorFlagH) ? this.errorFlagH : !this.errorFlagH;
            return ;
        }

        this.setFlagsButton(2);
        values['type_user']  = 1;
        values['is_company'] = 0;

        this.apiService.postRequest('/api/auth/create', JSON.stringify(values))
                       .subscribe((res: any) => {
                           // TODO: Implement observables
                            if (res['body'].status == 'OK' && res['body'].code == 200) {
                                let registerData = res['body'].data;
                                this.redirectUser(registerData.user, registerData.id_user)
                            }
                        
                            if (res['body'].status == 'ERROR' && res['body'].code == 400) {
                                this.errorMessage = res['body'].message;
                                this.errorStatus  = res['body'].status;
                                this.errorCode    = res['body'].code;
                            }
                       },
                       (error: any) => {
                           // TODO: What code should be put here?
                       },
                       () => {
                           // TODO: What code should be put here?
                       });
    }

    /**
     * Method for removing the data from the message error
     * displayes in case has been displayes on the forms
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @param formId number
     * @platform contractorsbids
     */
    private setFlagsButton(formId): void {
        this.errorCode      = 0;
        this.errorStatus    = '';

        if (formId == 1) {
            this.errorFlagC     = false;
            this.successFlagC   = true;
        }

        if (formId == 2) {
            this.errorFlagH      = false;
            this.successFlagH    = true;
        }
    }

    /**
     * Method for redirect to specific URL once the user
     * has finished the registration process
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    redirectUser(name, id){
        this.router.navigate(['/message-information'], { queryParams: {
                                                                        user: name,
                                                                        id: id
                                                                      }
                                                       });
    }

    /**
     * Method will clean the fiels if the user clicks
     * the clean button from contractor form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearFieldsC(): void {
        this.submittedFormC = false;
        this.errorFlagC     = false;
        this.successFlagC   = false;

        this.contractorForm.reset();
        this.contractorForm.value.cTermsContractor = false;
    }

    /**
     * Method will clean the fiels if the user clicks
     * the clean button from home form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearFieldsH(): void {
        this.submittedFormH = false;
        this.errorFlagH     = false;
        this.successFlagH   = false;

        this.homeOwnerForm.reset();
        this.homeOwnerForm.value.cTermsHome = false;
    }

    /**
     * Check if the flag is set to true
     * for displating success or error messages
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @params typeError string
     */
    isMessageDisplayed(typeError: string): boolean {
        if (typeError == 'errorC') {
            return this.errorFlagC;
        }

        if (typeError == 'errorH') {
            return this.errorFlagH;
        }
    }

    /**
     * Method will be used to dismiss a success message,
     * due to will appear once the process of send the
     * form has been done successfully with no error
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @params void
     */
    dismissMessage(message: IMessage): void {
        message.dismiss();
    }

    /**
     * Method to get the elements of the contractor form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    get fc() {
        return this.contractorForm.controls;
    }

    /**
     * Method to get the elements of the home form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    get fh() {
        return this.homeOwnerForm.controls;
    }

    /**
     * Method for activate the contract tab
     * and the user can filled up the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    activateContractorTab(): boolean {
        this.submittedFormC = false;
        this.errorFlagC     = false;
        this.errorCode      = 0;
        this.errorStatus    = '';
        this.errorMessage   = '';

        return false;
    }

    /**
     * Method for activate the home tab
     * and the user can filled up the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    activateHomeOwnerTab(): boolean {
        this.submittedFormH = false;
        this.errorFlagH     = false;
        this.errorCode      = 0;
        this.errorStatus    = '';
        this.errorMessage   = '';

        return false;
    }

    /**
     * Method will reset the values used to reset the values of the
     * flags once the user click the reset button
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clearErrors(): void {
        this.errorStatus    = '';
        this.errorCode      = 0;
        this.errorFlagH     = false;
        this.errorFlagC     = false;
        this.submittedFormH = false;
        this.submittedFormC = false;
    }

    /**
     * Method to open the modal with the information
     * of terms and conditions, once the user clicks
     * on the link "terms and conditions"
     * 
     * @param id string
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids   
     */
    termsConditionsInfoOpen(id: string) : void {
        this.modalService.open(id);
    }

    /**
     * Close the terms and conditions once the user clicks on the 
     * close button
     * 
     * @param id string
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids 
     */
    termsConditionsInfoClose(id: string): void {
        this.modalService.close(id);
    }
}
