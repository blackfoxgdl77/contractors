import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllProjectsBidsComponent } from './view-all-projects-bids.component';

describe('ViewAllProjectsBidsComponent', () => {
  let component: ViewAllProjectsBidsComponent;
  let fixture: ComponentFixture<ViewAllProjectsBidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAllProjectsBidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllProjectsBidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
