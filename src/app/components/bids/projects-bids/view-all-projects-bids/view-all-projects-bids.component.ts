import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';
import { IMessage } from 'ng2-semantic-ui';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-view-all-projects-bids',
    templateUrl: './view-all-projects-bids.component.html',
    styleUrls: ['./view-all-projects-bids.component.scss']
})
export class ViewAllProjectsBidsComponent implements OnInit {
    paramId         : any;
    codeResponse    : number = 0;
    statusResponse  : string = '';
    messageResponse : string = '';
    myBidsActive    : [];
    totalRecords    : number;

    title  : string = "My Projects Bids";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'My Projects Bids', link: '', isLink: false }
    ];
    steps : Object[] = [
        { title: 'Review the Bids', icons: 'eye', description: 'Review the bids published', completed: 'completed' },
        { title: 'Choose a Bid', icons: 'money', description: 'Choose the better bid' },
        { title: 'Get it done!', icons: 'check circle outline', description: 'Work the project once you selected the bid!' }
    ];

    constructor(private api: ApiService,
                private auth: AuthService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {

        this.paramId = this.auth.number;

        this.api.getRequest(`/api/bids/${this.paramId}/open/own`)
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let responseData = res['body'].data;

                        this.myBidsActive    = responseData;
                        this.totalRecords    = this.myBidsActive.length;
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method will be used to close the baners are displayed on the
     * list
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message:IMessage)
    {
        this.messageResponse = '';
        this.statusResponse  = '';
        this.codeResponse    = 0;
    }

}
