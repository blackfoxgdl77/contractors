import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-projects-bids',
    templateUrl: './projects-bids.component.html',
    styleUrls: ['./projects-bids.component.scss']
})
export class ProjectsBidsComponent implements OnInit {

    constructor(private api: ApiService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }
}
