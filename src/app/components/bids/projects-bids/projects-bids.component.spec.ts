import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsBidsComponent } from './projects-bids.component';

describe('ProjectsBidsComponent', () => {
  let component: ProjectsBidsComponent;
  let fixture: ComponentFixture<ProjectsBidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsBidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsBidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
