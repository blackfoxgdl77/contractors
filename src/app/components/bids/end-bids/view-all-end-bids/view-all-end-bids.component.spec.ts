import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllEndBidsComponent } from './view-all-end-bids.component';

describe('ViewAllEndBidsComponent', () => {
  let component: ViewAllEndBidsComponent;
  let fixture: ComponentFixture<ViewAllEndBidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAllEndBidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllEndBidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
