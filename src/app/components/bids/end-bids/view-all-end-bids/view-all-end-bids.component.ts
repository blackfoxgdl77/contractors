import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-view-all-end-bids',
    templateUrl: './view-all-end-bids.component.html',
    styleUrls: ['./view-all-end-bids.component.scss']
})
export class ViewAllEndBidsComponent implements OnInit {
    paramId         : any = 0;
    endbids         : [];
    flagSuccess     : boolean = false;
    flagError       : boolean = false;
    codeResponse    : number;
    statusResponse  : string;
    messageResponse : string;
    headerResponse  : string;

    title  : string = "My End Bids - Lists";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'My End Bids', link: '/bids/end-bids/view-end-bids', isLink: true },
        { text: 'View All End Bids', link: '', isLink: false }
    ];
    steps : Object[] = [
        { title: 'Review the Bids', icons: 'eye', description: 'Review the bids published', completed: 'completed' },
        { title: 'Make a Bid', icons: 'money', description: 'Make a bid to gain the project' },
        { title: 'Get it done!', icons: 'check circle outline', description: 'Work the project once you selected the bid!' }
    ];

    constructor(private api: ApiService,
                private auth: AuthService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
        this.api.getRequest(`/api/get/${this.paramId}/${this.auth.number}/projects/bids`)
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        let returnedData = res['body'].data;

                        this.endbids         = returnedData;
                        this.codeResponse    = res['body'].code;
                        this.messageResponse = res['body'].message;
                        this.statusResponse  = res['body'].status;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.codeResponse    = res['body'].code;
                        this.messageResponse = res['body'].message;
                        this.statusResponse  = res['body'].status;
                        this.flagError       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be implemented?
                },
                () => {
                    // TODO: What code should be implemented?
                });
    }

    /**
     * Function will be used to check if the error is valid
     * and should be displayed the message error
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    checkError(): boolean {
        if ((this.codeResponse == 400 && this.statusResponse == 'ERROR' && this.flagError) || (this.flagError)) {
            return true;
        }

        return false;
    }

}
