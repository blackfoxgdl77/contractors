import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEndBidsComponent } from './view-end-bids.component';

describe('ViewEndBidsComponent', () => {
  let component: ViewEndBidsComponent;
  let fixture: ComponentFixture<ViewEndBidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEndBidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEndBidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
