import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-end-bids',
    templateUrl: './end-bids.component.html',
    styleUrls: ['./end-bids.component.scss']
})
export class EndBidsComponent implements OnInit {

    constructor(private api: ApiService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

}
