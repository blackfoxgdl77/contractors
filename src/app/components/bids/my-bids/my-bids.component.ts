import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-my-bids',
    templateUrl: './my-bids.component.html',
    styleUrls: ['./my-bids.component.scss']
})
export class MyBidsComponent implements OnInit {

    constructor(private api: ApiService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }
}
