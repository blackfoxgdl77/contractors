import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBidsPostsComponent } from './view-bids-posts.component';

describe('ViewBidsPostsComponent', () => {
  let component: ViewBidsPostsComponent;
  let fixture: ComponentFixture<ViewBidsPostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBidsPostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBidsPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
