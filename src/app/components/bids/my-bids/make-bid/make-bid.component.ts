import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';
import { IMessage } from 'ng2-semantic-ui';
import { AuthService } from 'src/app/services/auth.service';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
    selector: 'app-make-bid',
    templateUrl: './make-bid.component.html',
    styleUrls: ['./make-bid.component.scss']
})
export class MakeBidComponent implements OnInit {
    paramId         : any     = 0;
    bids            : [];
    addBid          : [];
    newestbids      : number;
    codeResponse    : number;
    statusResponse  : string;
    messageResponse : string;
    headerResponse  : string;
    flagSuccess     : boolean = false;
    flagError       : boolean = false;
    makebid         : number  = null;

    title  : string = "Make a Bid";
    breads : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'My Bids', link: '/bids/my-bids/view-all-bids', isLink: true },
        { text: 'Make a Bid', link: '', isLink: false }
    ];
    steps : Object[] = [
        { title: 'Review the Bids', icons: 'eye', description: 'Review the bids published', completed: 'completed' },
        { title: 'Make a Bid', icons: 'money', description: 'Make a bid to gain the project' },
        { title: 'Get it done!', icons: 'check circle outline', description: 'Work the project once you selected the bid!' }
    ];

    constructor(private api: ApiService,
                private activatedRoute: ActivatedRoute,
                private auth: AuthService) { }

    ngOnInit() {
        this.paramId = this.activatedRoute.snapshot.paramMap.get('project_id');
        this.api.getRequest(`/api/get/${this.paramId}/${this.auth.number}/projects/bids`)
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let returnedData = res['body'].data;

                        this.bids            = returnedData;
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;

                        this.getCurrentBid();
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.flagError       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Method will be used to get the current bid active 
     * done for every user and could validate the newest
     * bid with the current bid
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private getCurrentBid() {
        for (let valuesbids in this.bids) {
            if (this.bids[valuesbids]['status'] == 1) {
                this.newestbids = this.bids[valuesbids]['offer'];

                break;
            }
        }
    }

    /**
     * Method will be used to close the banners are displayed on the
     * list
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public dismiss(message:IMessage)
    {
        this.flagError       = false;
        this.flagSuccess     = false;
        this.codeResponse    = 0;
        this.statusResponse  = '';
        this.messageResponse = '';
    }

    /**
     * Method will be used to send the offer sent by the user, should be bigger
     * than the current offer, otherwise will not be allowed to offer
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    makeABid(): boolean {
        this.flagError   = false;
        this.flagSuccess = false;

        if (+this.makebid == 0 || +this.makebid == null) {
            this.messageResponse = "The Bid is invalid";
            this.flagError       = true;

            return false;
        }

        if (+this.makebid <= +this.newestbids) {
            this.messageResponse = "Invalid Bid! Your Bid MUST be greater than the current Bid!";
            this.flagError       = true;

            return false;
        }

        this.messageResponse = '';
        let values = { postId: this.paramId,
                       offer : this.makebid };

        this.api.postRequest(`/api/bids/make/offer`, JSON.stringify(values))
                .subscribe((res: any) => {
                    // TODO: Implementing observables
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        let returnedData = res.body['data'];

                        localStorage.setItem('userT1', res.headers.get('_token_sec'));
                        localStorage.setItem('userT2', res.headers.get('_token_log'));
                        localStorage.setItem('number', res.headers.get('_token_user'));

                        this.addBid          = returnedData;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.headerResponse  = res['body'].header;
                        this.codeResponse    = res['body'].code;
                        this.flagSuccess     = true;
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR')
                    {
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                        this.codeResponse    = res['body'].code;
                        this.flagError       = true;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });

        return true;
    }

    /**
     * Method will be used to cancel bids if the user
     * suddenly is not sure to send the bid
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    cancelBid(): void {
        this.makebid = null;
    }

    /**
     * Function will be used to check if the error is valid
     * and should be displayed the message error
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    checkError(): boolean {
        if ((this.codeResponse == 400 && this.statusResponse == 'ERROR' && this.flagError) || (this.flagError)) {
            return true;
        }

        return false;
    }
}
