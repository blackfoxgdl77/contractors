import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllEndProjectsBidsComponent } from './view-all-end-projects-bids.component';

describe('ViewAllEndProjectsBidsComponent', () => {
  let component: ViewAllEndProjectsBidsComponent;
  let fixture: ComponentFixture<ViewAllEndProjectsBidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAllEndProjectsBidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllEndProjectsBidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
