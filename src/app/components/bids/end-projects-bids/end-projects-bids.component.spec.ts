import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndProjectsBidsComponent } from './end-projects-bids.component';

describe('EndProjectsBidsComponent', () => {
  let component: EndProjectsBidsComponent;
  let fixture: ComponentFixture<EndProjectsBidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndProjectsBidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndProjectsBidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
