import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ApiService } from 'src/app/services/api.service';
import { DinamycData } from 'src/app/models/dinamyc-data';
import { Post } from 'src/app/models/post';
import { EmbedVideoService } from 'ngx-embed-video';
import { IMessage } from 'ng2-semantic-ui';
import { SliderCarouselComponent } from 'src/app/customComponents/slider-carousel/slider-carousel.component';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
    posts           : Post[];
    totalPosts      : number;
    username        : string;
    codeResponse    : number;
    statusResponse  : string;
    messageResponse : string;
    elements        : any = [];

    steps : Object[] = [
        { title: 'Post your Project', icons: 'pencil alternate', description: 'Post the Project you want to offer to the users' },
        { title: 'Get Bids', icons: 'money bill alternate outline', description: 'Get a lot of bids from users registered'},
        { title: 'Get it done!', icons: 'check circle outline', description: 'Select the best bid for your project and start to work!' }
    ];
 
    constructor(private auth: AuthService, 
                private api: ApiService,
                private videos: EmbedVideoService) { }

    ngOnInit() {
        this.getAllBanners();
        this.getAllRecentProjects();
    }

    ngOnDestroy() {
        // TODO: Implement unsubscribe
    }
    
    demo() {
        (<any>$('.ui.basic.modal')).modal('show');
    }

    /**
     * Will be used to get all the banners once the 
     * angular has been initialize
     * 
     * @author Ruben Alonso Cortes Mendoza<ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    getAllBanners(): void {
        // get banners will be displayed on the main page
        this.api.getRequest(`/api/website/banners`)
                .subscribe((res: any) => {
                    // TODO: Move for implement observers
                    let returnedData = res['body'].data;

                    for(let i = 0; i < returnedData.length; i++) {
                        let object = { 'thumb'       : returnedData[i].file_sliders,
                                       'src'         : returnedData[i].file_sliders,
                                       'description' : returnedData[i].description,
                                       'url_thumb'   : (returnedData[i].type_file == 2) ? this.videos.embed(returnedData[i].url, { attr : { width: 1200, height: 500}}) : '',
                                       'url'         : returnedData[i].url,
                                       'status'      : returnedData[i].status,
                                       'type_file'   : returnedData[i].type_file };
                        this.elements.push(object);
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

    /**
     * Will return the six project most recent created in the platform
     * for displaying in the main page
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    getAllRecentProjects(): void {
        // get most recent projects to display on the main page
        this.api.getRequest('/api/website/recent/projects')
                .subscribe((res: any) => {
                    // TODO: Implement Observers, modify code
                    if (res['body'].code == 200 && res['body'].status == 'OK')
                    {
                        this.posts           = res['body'].data;
                        this.totalPosts      = this.posts.length;
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                    }

                    if (res.body['code'] == 400 && res.body['status'] == 'ERROR') 
                    {
                        this.codeResponse    = res['body'].code;
                        this.statusResponse  = res['body'].status;
                        this.messageResponse = res['body'].message;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }

}
