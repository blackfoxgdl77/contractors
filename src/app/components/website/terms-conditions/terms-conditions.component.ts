import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { BreadcrumbsComponent } from 'src/app/components/breadcrumbs/breadcrumbs.component';
import { DinamycData } from 'src/app/models/dinamyc-data';
import { EmbedVideoService } from 'ngx-embed-video';
import { IMessage } from 'ng2-semantic-ui';

@Component({
    selector: 'app-terms-conditions',
    templateUrl: './terms-conditions.component.html',
    styleUrls: ['./terms-conditions.component.scss']
})
export class TermsConditionsComponent implements OnInit, OnDestroy {
    infoData     : DinamycData[];
    videoFrame   : any;
    errorStatus  : string;
    errorCode    : number;
    errorMessage : string;

    title    : string = "How It Works";
    breads   : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'How It Works', link: '', isLink: false }
    ];

    constructor(private api: ApiService, private videos: EmbedVideoService) { }

    ngOnInit() {
        this.getAllTerms();
    }

    ngOnDestroy(): void {
        // TODO: Implement unsubscribe
    }

    /**
     * 
     */
    getAllTerms(): void {
        this.api.getRequest('/api/website/termsConditions')
                .subscribe( (res: any) => {
                    // TODO: Implement observers
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        this.infoData   = res['body'].data[0];
                        this.videoFrame = (this.infoData['type_file'] == 2) ? this.videos.embed(this.infoData['url'], { attr: { width: 800, height: 400}}) : '';
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.errorMessage = res['body'].message;
                        this.errorCode    = res['body'].code;
                        this.errorStatus  = res['body'].status;
                    }
                },
                (error: any) => {
                    // TODO: What code should be implemented?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }
}
