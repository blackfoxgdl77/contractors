import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { IMessage } from 'ng2-semantic-ui';
import { BreadcrumbsComponent } from 'src/app/components/breadcrumbs/breadcrumbs.component';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
    contactForm   : FormGroup;
    errorFlag     : boolean = false;
    successFlag   : boolean = false;
    submittedForm : boolean = false;

    title  : string = "Contact Us!";
    breads : Object[] = [
        { text: 'Home', link: "/", isLink: true },
        { text: 'Contact Us!', link: '', isLink: false }
    ];

    constructor(private contactBuilder: FormBuilder, private apiService: ApiService) { }

    ngOnInit() {
        this.contactFormValidation();
    }

    /**
     * Method will contain all the validations for
     * the form and check if could be sent or not
     * depending on if the user filled in the form or not
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    contactFormValidation(): void {
        this.contactForm = this.contactBuilder.group({
            subject  : ['', [Validators.required]],
            email    : ['', [Validators.required, Validators.email]],
            comments : ['', [Validators.required, Validators.minLength(4), Validators.maxLength(600)]],
            phone    : ['']
        });
    }

    /**
     * Method will clear the fields from the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    clearFields(): void {
        this.errorFlag   = false;
        this.successFlag = false;

        this.contactForm.value.subject  = '';
        this.contactForm.value.phone    = '';
        this.contactForm.value.email    = '';
        this.contactForm.value.comments = '';
    }

    /**
     * Method will use for sending emails
     * once the contact form has been filled in
     * by the user
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    contact(): boolean {
        this.submittedForm = true;

        if (this.contactForm.invalid)
        {
            this.errorFlag = (this.errorFlag) ? this.errorFlag : !this.errorFlag;
            return ;
        }

        this.errorFlag = false;
        this.successFlag = true;
    }

    /**
     * Check if the flag is set to true
     * for displating success or error messages
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @params typeError string
     */
    isMessageDisplayed(typeError: string): boolean {
        if (typeError == 'error') {
            return this.errorFlag;
        }

        if (typeError == 'success') {
            return this.successFlag;
        }
    }

    /**
     * Method will be used to dismiss a success message,
     * due to will appear once the process of send the
     * form has been done successfully with no error
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @params void
     */
    dismissMessage(message: IMessage): void {
        message.dismiss();
    }

    /**
     * Method to get the elements of the form
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     *
     * @param void
     */
    get f() {
        return this.contactForm.controls;
    }
}
