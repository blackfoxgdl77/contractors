import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { BreadcrumbsComponent } from 'src/app/components/breadcrumbs/breadcrumbs.component';
import { DinamycData } from 'src/app/models/dinamyc-data';
import { EmbedVideoService } from 'ngx-embed-video';
import { IMessage } from 'ng2-semantic-ui';

@Component({
    selector: 'app-about-us',
    templateUrl: './about-us.component.html',
    styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit , OnDestroy{
    infoData     : DinamycData[];
    videoFrame   : any;
    errorStatus  : string;
    errorCode    : number;
    errorMessage : string;
    
    title    : string = "About Us";
    breads   : Object[] = [
        { text: 'Home', link: '/', isLink: true },
        { text: 'About Us', link: '', isLink: false }
    ];

    constructor(private api: ApiService, private videos: EmbedVideoService) { }

    ngOnInit() {
        this.getAllAboutData();
    }

    ngOnDestroy(): void {
        // TODO: Implement unsubscribe
    }

    getAllAboutData(): void {
        this.api.getRequest('/api/website/aboutUs')
                .subscribe( (res: any) => {
                    // TODO: Implemenet code for observers
                    if (res['body'].code == 200 && res['body'].status == 'OK') {
                        this.infoData   = res['body'].data[0];
                        this.videoFrame = (this.infoData['type_file'] == 2) ? this.videos.embed(this.infoData['url'], { attr: { width: 800, height: 400}}) : '';
                    }

                    if (res['body'].code == 400 && res['body'].status == 'ERROR') {
                        this.errorMessage = res['body'].message;
                        this.errorCode    = res['body'].code;
                        this.errorStatus  = res['body'].status;
                    }
                },
                (error: any) => {
                    // TODO: What code should be put here?
                },
                () => {
                    // TODO: What code should be put here?
                });
    }
}
