import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

    /**
     * Method will return a string cut with the limit set in the
     * parameter passed once the pipe is used on the template. This 
     * pipe adds '...' string in the string cut
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @param value: any
     * @param args: any
     * @version 1.0
     * @platform contractorsbids
     */
    transform(value: any, args?: any): any {
        let limit = parseInt(args);

        return value.length > limit ? value.substring(0, limit) + '...' : value;
    }

}
