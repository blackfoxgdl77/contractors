import { ComponentModalConfig, ModalSize } from 'ng2-semantic-ui';
import { AcceptRejectModalComponent } from 'src/app/customComponents/AcceptRejectModalComponent';

export class AcceptRejectModal extends ComponentModalConfig<IAcceptRejectModalContext, void, void> 
{
    constructor(title: string, body: string, accept: string, deny: string)
    {
        super(AcceptRejectModalComponent, { title, body, accept, deny }, false);

        this.transitionDuration = 200;
        this.size               = ModalSize.Small;
    }
}