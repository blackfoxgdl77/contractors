import { BaseModel } from './base-model';

export class UserInformation extends BaseModel {
  payment_method    : string;
  phone             : string;
  bbb_account       : string;
  bbb_accredited    : string;
  raiting           : string;
  wcb_number        : string;
  address           : string;
  user              : number;
}
