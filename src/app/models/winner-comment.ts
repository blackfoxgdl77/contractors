import { BaseModel } from './base-model';

export class WinnerComment extends BaseModel {
  comment       : string;
  owner         : number;
  winner        : number;
  project_id    : number;
}
