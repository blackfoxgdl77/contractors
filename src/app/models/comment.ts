import { BaseModel } from './base-model';

export class Comment extends BaseModel {
  user    : number;
  post    : number;
  owner   : number;
  comment : string;
  publish : number;

}
