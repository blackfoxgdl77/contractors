import { BaseModel } from './base-model';

export class Attachment extends BaseModel {
  name            : string;
  path            : string;
  isImage         : boolean;
  fileExtension   : string;
  post            : number;

  constructor() {
    super();
    this.date_created = new Date().toLocaleString();
    this.date_updated = new Date().toLocaleString();
  }

}
