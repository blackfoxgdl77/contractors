import { BaseModel } from './base-model';

export class PaymentHistory extends BaseModel {
  user          : number;
  post          : number;
  amount        : number;
  status        : string;
  cart          : string;
  transaction   : string;
  date_payment  : string;

}
