import { BaseModel } from './base-model';

export class JobCategory extends BaseModel {
  name  : string;
}
