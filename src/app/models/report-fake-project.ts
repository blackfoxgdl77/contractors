import { BaseModel } from './base-model';

export class ReportFakeProject extends BaseModel {
  fake_project_id   : number;
  reported_user     : number;
}
