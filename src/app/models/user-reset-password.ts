import { BaseModel } from './base-model';

export class UserResetPassword extends BaseModel {
  token_reset   : string;
  active        : boolean;
  user          : number;
  user_mail     : string;
}
