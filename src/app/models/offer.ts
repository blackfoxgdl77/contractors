import { BaseModel } from './base-model';

export class Offer extends BaseModel {
  user            : number;
  auctioner       : number;
  post            : number;
  bidder          : number;
  active_auction  : boolean;

}
