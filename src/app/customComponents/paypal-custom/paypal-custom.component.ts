import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ApiService } from 'src/app/services/api.service';
import { IPayPalConfig, ICreateOrderRequest, IPlatformFee } from 'ngx-paypal';

@Component({
    selector: 'app-paypal-custom',
    templateUrl: './paypal-custom.component.html',
    styleUrls: ['./paypal-custom.component.scss']
})
export class PaypalCustomComponent implements OnInit {
    @Input() postId;
    @Input() postPayments;

    payPalConfig?   : IPayPalConfig;
    funding_source  : string;
    merchant_id     : string;
    currency_code   : string;
    codeResponse    : number;
    statusResponse  : string  = '';
    messageResponse : string  = '';
    headerResponse  : string  = '';
    flagSuccess     : boolean = false;
    flagError       : boolean = false;

    constructor(private api: ApiService,
                private auth: AuthService) { }

    ngOnInit() {
        this.paypalConfiguration();
    }

    private paypalConfiguration(): void {
        this.payPalConfig = {
            currency: 'MXN',
            clientId: 'ATpQbrHs2rtQuJvfjfDQPjncHIusx0zJ3RtcDPruWKmdr-OaK_Ph8fxIkDAUHN4nB92en9kEjwHf8Rti',
            createOrderOnClient: (data) => <ICreateOrderRequest>{
                intent: 'CAPTURE',
                purchase_units: [
                    {
                        amount: {
                            currency_code: 'MXN',
                            value: '1',
                            breakdown: {
                                item_total: {
                                    currency_code: 'MXN',
                                    value: '1'
                                }
                            }
                        },
                        items: [
                            {
                                name: 'Test',
                                quantity: '1',
                                //category: 'LAWS',
                                unit_amount: {
                                    currency_code: 'MXN',
                                    value: '1',
                                },
                            }
                        ]
                    }
                ]
            },
            advanced: {
                commit: 'true'
            },
            style: {
                label: 'paypal',
                layout: 'vertical',
                size: 'responsive'
            },
            onApprove: (data, actions) => {
                actions.order.get().then(details => {
                    this.currency_code = details.purchase_units[0].amount.currency_code;
                    this.merchant_id = details.purchase_units[0].payee.merchant_id;
                });
            },
            onClientAuthorization: (data) => {
                //call the method will be used to save data
                this.authorizationPayment(data.payer.payer_id,
                                          data.id,
                                          this.merchant_id,
                                          this.currency_code,
                                          data.status,
                                          this.funding_source,
                                          data.payer.name.given_name,
                                          data.payer.email_address,
                                          data.payer.name.surname);

                //this.showSuccess = true;
            },
            onCancel: (data, actions) => {
                this.cancelPayment(data);
            },
            onError: err => {
            },
            onClick: (data, actions) => {
                this.funding_source = data.fundingSource;
            },
        };
    }

    /**
     * Action will be called once the user cancel the payment action
     * 
     * @param object order
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private cancelPayment(order) {
        let obj = { 'order'  : order.orderID,
                    'postId' : this.postId,
                    'amount' : this.postPayments };

        this.api.postRequest(`/api/payment/cancel`, JSON.stringify(obj))
                .subscribe(res => {
                    if (res.body['status'] === 'OK' && res.body['code'] === 200) {
                        let dataReturned = res.body['data'];

                        this.codeResponse    = res.body['code'];
                        this.statusResponse  = res.body['status'];
                        this.messageResponse = res.body['message'];
                        this.headerResponse  = res.body['header'];
                    }

                    if (res.body['status'] === 'ERROR' && res.body['code'] === 400) {
                        this.codeResponse    = res.body['code'];
                        this.statusResponse  = res.body['status'];
                        this.messageResponse = res.body['message'];
                    }
                })
    }

    /**
     * Method will be used to save the transactions once has been completed
     * 
     * @param 
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    private authorizationPayment(transactionId: string, 
                                 orderId: string, 
                                 merchantId: string, 
                                 currency: string,
                                 status: string,
                                 payment_source: string,
                                 payer_name: string,
                                 payer_email: string,
                                 payer_surname: string): void {
        let obj = { 'transaction'    : transactionId,
                    'order_number'   : orderId,
                    'merchant_id'    : merchantId,
                    'currency'       : currency,
                    'status'         : status,
                    'payment_source' : payment_source,
                    'payer_name'     : payer_name,
                    'payer_email'    : payer_email,
                    'payer_surname'  : payer_surname};

        this.api.postRequest(`/api/payment/confirm`, JSON.stringify(obj))
                .subscribe(res => {
                    if (res.body['code'] === 200 && res.body['status'] === 'OK') {
                        let returnData = res.body['data'];

                        this.codeResponse    = res.body['code'];
                        this.statusResponse  = res.body['status'];
                        this.headerResponse  = res.body['header'];
                        this.messageResponse = res.body['message'];
                        this.flagSuccess     = true;
                    }

                    if (res.body['code'] === 400 && res.body['status'] === 'ERROR') {
                        this.flagError       = true;
                        this.codeResponse    = res.body['code'];
                        this.statusResponse  = res.body['status'];
                        this.messageResponse = res.body['message'];
                    }
                })
    }

}
