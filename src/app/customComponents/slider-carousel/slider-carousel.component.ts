import { Component, OnInit, Input } from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';
import { Lightbox, LightboxEvent } from 'ngx-lightbox';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-slider-carousel',
    templateUrl: './slider-carousel.component.html',
    styleUrls: ['./slider-carousel.component.scss']
})
export class SliderCarouselComponent implements OnInit {
    @Input() elements: any;

    element    : any;
    isPlayStop : boolean;

    carouselOptions = {
        loop: true,
        margin: 25,
        autoHeight: true,
        items: 1,
        nav: true,
        autoplay: true,
        autoplayHoverPause: false,
        navText: ["<i class='fa fa-caret-left'></i>", "<i class='fa fa-caret-right'></i>"],
        responsive: {
            0: {
                items: 1,
                nav: true,
                loop: true
            },
            600: {
                items: 1,
                nav: true,
                loop: true
            },
            1000: {
                items: 1,
                nav: true,
                loop: true
            }
        }
    };

    private subscription: Subscription;

    constructor(private _lightbox: Lightbox, private _lightboxEvent: LightboxEvent) { }

    ngOnInit() {
    }

    openVideo() {
        console.log("holas");
    }

    /**
     * Used to validate if the user click on the banner of slider, must
     * stop until click close the banner and activate again the autoplay
     * 
     * @param element any
     * @return void
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    clickOpen(index: number) : void {
        this.isPlayStop = !this.isPlayStop;
        this._lightbox.open(this.elements, index, {
            'enableTransition': false
        });

        if (this.isPlayStop) {
            $('.owl-carousel').trigger('stop.owl.autoplay');
        } else {
            $('.owl-carousel').trigger('play.owl.autoplay');
        }
    }

    close(): void {
        // close lightbox programmatically
        this._lightbox.close();
      }

    /**
     * Close modal once the user click the close button
     * and will activate the loop of the slider
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    closeModal(): void {
        console.log("holas");
        //this.isPlayStop = !this.isPlayStop;
        //$('.owl-carousel').trigger('play.owl.autoplay');
    }
}
