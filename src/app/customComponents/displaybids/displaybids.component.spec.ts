import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplaybidsComponent } from './displaybids.component';

describe('DisplaybidsComponent', () => {
  let component: DisplaybidsComponent;
  let fixture: ComponentFixture<DisplaybidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplaybidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplaybidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
