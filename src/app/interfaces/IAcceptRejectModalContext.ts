interface IAcceptRejectModalContext {
    title : string;
    body  : string;
    accept: string;
    deny  : string;
}