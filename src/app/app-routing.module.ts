import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

import { WebsiteComponent } from './components/website/website.component';
import { MainComponent } from './components/website/main/main.component';
import { AboutUsComponent } from './components/website/about-us/about-us.component';
import { HowItWorksComponent } from './components/website/how-it-works/how-it-works.component';
import { TermsConditionsComponent } from './components/website/terms-conditions/terms-conditions.component';
import { ContactComponent } from './components/website/contact/contact.component';

import { AuthComponent } from './components/auth/auth.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { ResetPwdComponent } from './components/auth/reset-pwd/reset-pwd.component';
import { ChangePwdComponent } from './components/auth/change-pwd/change-pwd.component';

import { AdminsComponent } from './components/admins/admins.component';
import { AdminComponent } from './components/admins/admin/admin.component';
import { AdminBannersComponent } from './components/admins/admin-banners/admin-banners.component';
import { AdminHowItWorksComponent } from './components/admins/admin-how-it-works/admin-how-it-works.component';
import { AdminAboutUsComponent } from './components/admins/admin-about-us/admin-about-us.component';
import { AdminTermsConditionsComponent } from './components/admins/admin-terms-conditions/admin-terms-conditions.component';
import { ListUsersComponent } from './components/admins/list-users/list-users.component';
import { JobsReportedComponent } from './components/admins/jobs-reported/jobs-reported.component';

import { UsersComponent } from './components/users/users.component';

import { PostsComponent } from './components/posts/posts.component';
import { CreateComponent as CreatePost } from './components/posts/create/create.component';
import { UpdateComponent as UpdatePost } from './components/posts/update/update.component';
import { ListComponent as PostDashboard } from './components/posts/list/list.component';
import { ViewComponent as ViewPost } from './components/posts/view/view.component';

import { CommentsComponent } from './components/posts/view/comments/comments.component';

import { ListComponent } from './components/admins/admin-about-us/list/list.component';
import { CreateComponent } from './components/admins/admin-about-us/create/create.component';
import { UpdateComponent } from './components/admins/admin-about-us/update/update.component';
import { ListComponent as ListWorks } from './components/admins/admin-how-it-works/list/list.component';
import { CreateComponent as CreateWorks } from './components/admins/admin-how-it-works/create/create.component';
import { UpdateComponent as UpdateWorks } from './components/admins/admin-how-it-works/update/update.component';
import { ListComponent as ListTerms } from './components/admins/admin-terms-conditions/list/list.component';
import { CreateComponent as CreateTerms } from './components/admins/admin-terms-conditions/create/create.component';
import { UpdateComponent as UpdateTerms } from './components/admins/admin-terms-conditions/update/update.component';
import { ListComponent as ListBanners } from './components/admins/admin-banners/list/list.component';
import { CreateComponent as CreateBanners } from './components/admins/admin-banners/create/create.component';
import { UpdateComponent as UpdateBanners } from './components/admins/admin-banners/update/update.component';
import { ListAllJobsReportedComponent } from './components/admins/jobs-reported/list-all-jobs-reported/list-all-jobs-reported.component';
import { ViewJobReportedComponent } from './components/admins/jobs-reported/view-job-reported/view-job-reported.component';

import { PersonalInformationComponent } from './components/users/personal-information/personal-information.component';
import { MessagesPagesComponent } from './components/messages/messages-pages/messages-pages.component';
import { ViewComponent } from './components/admins/admin-about-us/view/view.component';
import { ViewComponent as ViewBanners } from './components/admins/admin-banners/view/view.component';
import { ViewComponent as ViewWorks } from './components/admins/admin-how-it-works/view/view.component';
import { ViewComponent as ViewTerms } from './components/admins/admin-terms-conditions/view/view.component';
import { WarningUsersComponent } from './components/admins/list-users/warning-users/warning-users.component';
import { ConfirmMessageComponent } from './components/messages/confirm-message/confirm-message.component';
import { ViewUsersListComponent } from './components/admins/list-users/view-users-list/view-users-list.component';
import { ListAllUsersComponent } from './components/admins/list-users/list-all-users/list-all-users.component';
import { BidsComponent } from './components/bids/bids.component';
import { EndBidsComponent } from './components/bids/end-bids/end-bids.component';
import { EndProjectsBidsComponent } from './components/bids/end-projects-bids/end-projects-bids.component';
import { MyBidsComponent } from './components/bids/my-bids/my-bids.component';
import { ProjectsBidsComponent } from './components/bids/projects-bids/projects-bids.component';
import { ViewAllBidsComponent } from './components/bids/my-bids/view-all-bids/view-all-bids.component';
import { MakeBidComponent } from './components/bids/my-bids/make-bid/make-bid.component';
import { ViewAllEndBidsComponent } from './components/bids/end-bids/view-all-end-bids/view-all-end-bids.component';
import { ViewAllProjectsBidsComponent } from './components/bids/projects-bids/view-all-projects-bids/view-all-projects-bids.component';
import { ViewAllEndProjectsBidsComponent } from './components/bids/end-projects-bids/view-all-end-projects-bids/view-all-end-projects-bids.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { ViewEndBidsComponent } from './components/bids/end-bids/view-end-bids/view-end-bids.component';
import { ViewOffersFinishedComponent } from './components/bids/end-projects-bids/view-offers-finished/view-offers-finished.component';
import { ViewAllMyBidsProjectsComponent } from './components/bids/projects-bids/view-all-my-bids-projects/view-all-my-bids-projects.component';

const routes: Routes = [
    // WebsiteComponent
    {
        path: '',
        component: WebsiteComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'main'
            },
            {
                path: 'main',
                component: MainComponent
            },
            {
                path: 'about-us',
                component: AboutUsComponent
            },
            {
                path: 'terms-conditions',
                component: TermsConditionsComponent
            },
            {
                path: 'how-it-works',
                component: HowItWorksComponent
            },
            {
                path: 'contact',
                component: ContactComponent
            }
        ]
    },

    // AuthComponent
    {
        path: 'auth',
        component: AuthComponent,
        children: [
            {
                path: 'login',
                component: LoginComponent
            },
            {
                path: 'register',
                component: RegisterComponent
            },
            {
                path: 'reset-password',
                component: ResetPwdComponent
            },
            {
                path: 'change-password/:reset_token/:reset_id',
                component: ChangePwdComponent
            },
            {
                path: 'logout',
                component: LoginComponent,
                canActivate: [AuthGuardService]
            },
            {
                path: 'confirm-account/:token_confirm',
                component: ConfirmMessageComponent
            }
        ]
    },

    // AdminsComponent
    {
        path: 'admin',
        component: AdminsComponent,
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'administrator',
                component: AdminComponent
            },
            {
                path: 'about-us',
                component: AdminAboutUsComponent,
                children: [
                    {
                        path: 'list',
                        component: ListComponent
                    },
                    {
                        path: 'create',
                        component: CreateComponent
                    },
                    {
                        path: 'update/:id',
                        component: UpdateComponent
                    },
                    {
                        path: 'view-about-us/single/:id',
                        component: ViewComponent
                    }
                ]
            },
            {
                path: 'banners',
                component: AdminBannersComponent,
                children: [
                    {
                        path: 'list',
                        component: ListBanners
                    },
                    {
                        path: 'create',
                        component: CreateBanners
                    },
                    {
                        path: 'update/:id',
                        component: UpdateBanners
                    },
                    {
                        path: 'view-banner/single/:id',
                        component: ViewBanners
                    }
                ]
            },
            {
                path: 'how-it-works',
                component: AdminHowItWorksComponent,
                children : [
                    {
                        path: 'list',
                        component: ListWorks
                    },
                    {
                        path: 'create',
                        component: CreateWorks
                    },
                    {
                        path: 'update/:id',
                        component: UpdateWorks
                    },
                    {
                        path: 'view-works/single/:id',
                        component: ViewWorks
                    }
                ]
            },
            {
                path: 'termsconditions',
                component: AdminTermsConditionsComponent,
                children: [
                    {
                        path: 'list',
                        component: ListTerms
                    },
                    {
                        path: 'create',
                        component: CreateTerms
                    },
                    {
                        path: 'update/:id',
                        component: UpdateTerms
                    },
                    {
                        path: 'view-terms/single/:id',
                        component: ViewTerms
                    }
                ]
            },
            {
                path: 'users-lists',
                component: ListUsersComponent,
                children: [
                    {
                        path: 'list-all-users',
                        component: ListAllUsersComponent
                    },
                    {
                        path: 'warning/:id',
                        component: WarningUsersComponent
                    },
                    {
                        path: 'view-user/:id/:type/personal',
                        component: ViewUsersListComponent
                    }
                ]
            }
        ]
    },

    // BidsComponents
    {
        path: 'bids',
        component: BidsComponent,
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'end-bids',
                component: EndBidsComponent,
                children: [
                    {
                        path: 'view-end-bids',
                        component: ViewEndBidsComponent
                    },
                    {
                        path: 'view-all-bids/:id',
                        component: ViewAllEndBidsComponent
                    }
                ]
            },
            {
                path: 'end-projects-bids',
                component: EndProjectsBidsComponent,
                children: [
                    {
                        path: 'view-all-end-projects',
                        component: ViewAllEndProjectsBidsComponent
                    },
                    {
                        path: 'view-offers/:id',
                        component: ViewOffersFinishedComponent
                    }
                ]
            },
            {
                path: 'my-bids',
                component: MyBidsComponent,
                children: [
                    {
                        path: 'view-all-bids',
                        component: ViewAllBidsComponent
                    },
                    {
                        path: 'make-bid/:project_id',
                        component: MakeBidComponent
                    }
                ]
            },
            {
                path: 'projects-bids',
                component: ProjectsBidsComponent,
                children: [
                    {
                        path: 'view-all-bids-project',
                        component: ViewAllProjectsBidsComponent
                    },
                    {
                        path: 'view-bids-per-project/:id',
                        component: ViewAllMyBidsProjectsComponent
                    }
                ]
            }
        ]
    },

    // JobsReportedComponent
    {
        path: 'jobs-reported',
        component: JobsReportedComponent,
        children: [
            {
                path: 'list-all-fake-jobs',
                component: ListAllJobsReportedComponent,
            },
            {
                path: 'view-job-reported/:fake_id/:project_id',
                component: ViewJobReportedComponent
            }
        ]
    },

    // UsersComponent
    {
        path: 'users',
        component: UsersComponent,
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'spi/:id',
                component: PersonalInformationComponent
            }
        ]
    },

    // PostsComponent
    {
        path: 'posts',
        component: PostsComponent,
        children: [
            {
                path: 'dashboard',
                component: PostDashboard
            },
            {
                path: 'create',
                component: CreatePost,
                canActivate: [AuthGuardService]
            },
            {
                path: 'update',
                component: UpdatePost,
                canActivate: [AuthGuardService]
            },
            {
                path: 'view/:id',
                component: ViewPost
            }
        ]
    },

    // CommentsComponent
    {
        path: 'comments',
        component: CommentsComponent,
    },

    // MessagesPagesComponent
    {
        path: 'message-information',
        component: MessagesPagesComponent
    },
    
    {
        path: '*.*',
        component: ErrorPageComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
