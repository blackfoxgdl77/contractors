import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse, HttpParams } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

const endpoint : string = "http://localhost:5000";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    private _refresh$ = new Subject<void>();

    constructor(private httpClient: HttpClient, private auth: AuthService) {
    }

    get refreshNeeded$() {
        return this._refresh$;
    }

    // get requests
    getRequest(url: string): Observable<any> {
        return this.httpClient.get<any>(`${endpoint}${url}`, 
                                        { headers : new HttpHeaders({ 'Content-Type' : 'application/json',
                                                                      '_token_sec'   : (this.auth.userT1) ? this.auth.userT1 : '',
                                                                      '_token_log'   : (this.auth.userT2) ? this.auth.userT2 : '',
                                                                      '_token_user'  : (this.auth.number) ? this.auth.number : '',
                                                                      '_token_adm'   : (this.auth.historyLogin) ? this.auth.historyLogin : '' }),
                                          observe: 'response',
                                          responseType : 'json' })
                               .pipe(
                                   tap(() => {
                                       this._refresh$.next();
                                   })
                               );
    }

    // post request
    postRequest(url: string, params: any): Observable<any> {
        return this.httpClient.post(`${endpoint}${url}`, params, 
                                         { headers : new HttpHeaders({ 'Content-Type' : 'application/json',
                                                                       '_token_sec'   : (this.auth.userT1) ? this.auth.userT1 : '',
                                                                       '_token_log'   : (this.auth.userT2) ? this.auth.userT2 : '',
                                                                       '_token_user'  : (this.auth.number) ? this.auth.number : '',
                                                                       '_token_adm'   : (this.auth.historyLogin) ? this.auth.historyLogin : '' }),
                                           observe: 'response',
                                           responseType : 'json' });
    }

    // delete request
    deleteRequest(url: string): Observable<any> {
        return this.httpClient.delete(`${endpoint}${url}`, 
                                           { headers : new HttpHeaders({ 'Content-Type' : 'application/json',
                                                                         '_token_sec'   : (this.auth.userT1) ? this.auth.userT1 : '',
                                                                         '_token_log'   : (this.auth.userT2) ? this.auth.userT2 : '',
                                                                         '_token_user'  : (this.auth.number) ? this.auth.number : '',
                                                                         '_token_adm'   : (this.auth.historyLogin) ? this.auth.historyLogin : '' }),
                                             observe: 'response',
                                             responseType : 'json' })
                               .pipe(
                                   tap(() => {
                                       this._refresh$.next();
                                   })
                               );
    }

    // update request
    updateRequest(url: string, params: any): Observable<any> {
        return this.httpClient.put(`${endpoint}${url}`, params, 
                                        { headers : new HttpHeaders({ 'Content-Type' : 'application/json',
                                                                      '_token_sec'   : (this.auth.userT1) ? this.auth.userT1 : '',
                                                                      '_token_log'   : (this.auth.userT2) ? this.auth.userT2 : '',
                                                                      '_token_user'  : (this.auth.number) ? this.auth.number : '',
                                                                      '_token_adm'   : (this.auth.historyLogin) ? this.auth.historyLogin : '' }),
                                          observe: 'response',
                                          responseType : 'json' });
    }
}
