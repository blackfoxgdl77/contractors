import { TestBed } from '@angular/core/testing';

import { EncryptAuthService } from './encrypt-auth.service';

describe('EncryptAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EncryptAuthService = TestBed.get(EncryptAuthService);
    expect(service).toBeTruthy();
  });
});
