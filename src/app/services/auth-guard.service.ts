import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { CanActivate, CanActivateChild, Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    constructor(private apiAuth: AuthService, private _router: Router) { }

    /**
     * Method will be enabled to check if in specific routes
     * can access due to the permission you have once logged in
     * 
     * @autor Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    canActivate(): boolean {
        if (this.apiAuth.loggedIn) {
            return true;
        }

        this._router.navigate(['/']);
        return false;
    }
}
