import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

const endpoint: string = "http://localhost:5000";

@Injectable({
    providedIn: 'root'
})
export class ImagesService {


    constructor(private apiImg : HttpClient,
                private auth   : AuthService) { }

    //POST REQUEST IMAGE
    postImage(url: string, params?: any): any {
        return this.apiImg.post(`${endpoint}${url}`, params);
        /*, { headers : new HttpHeaders({ 'Content-Type' : 'application/json',
                                                                                           '_token_sec'   : (this.auth.userT1) ? this.auth.userT1 : '',
                                                                                           '_token_log'   : (this.auth.userT2) ? this.auth.userT2 : '',
                                                                                           '_token_user'  : (this.auth.number) ? this.auth.number : '',
                                                                                           '_token_adm'   : (this.auth.historyLogin) ? this.auth.historyLogin : '' }),
                                                               observe: 'response',
                                                               responseType : 'json' });*/
    }
}
