import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor() { }

    /**
     * Remove the local storage created in the logged in and the user
     * won't be able to browse between pages
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    logout() {
        localStorage.removeItem('access_token');
        localStorage.removeItem('username');
        localStorage.removeItem('userT1');
        localStorage.removeItem('userT2');
        localStorage.removeItem('isAdmin');
        localStorage.removeItem('number');
        localStorage.removeItem('login_history');
    }

    /**
     * Check if the user is logged in in the platform or not,
     * in case the username is logged in return TRUE else FALSE
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public get loggedIn() {
        return (localStorage.getItem('access_token') !== null && 
                localStorage.getItem('username') !== null &&
                localStorage.getItem('isAdmin') !== null &&
                localStorage.getItem('userT1') !== null &&
                localStorage.getItem('userT2') !== null &&
                localStorage.getItem('number') !== null &&
                localStorage.getItem('login_history') !== null);
    }

    /**
     * Get the access token returned by the request once the user
     * has logged in
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorbids
     */
    public get access_token() {
        return localStorage.getItem('access_token');
    }

    /**
     * Get the refresh token returned by the request once the user
     * has logged in
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public get refresh_token() {
        return localStorage.getItem('refresh_token');
    }

    /**
     * Get the username returned by the request once the user
     * has logged in
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public get username() {
        return localStorage.getItem('username');
    }

    /**
     * Get the user t1 returned by the request once the user
     * has logged in
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public get userT1() {
        return localStorage.getItem('userT1');
    }

    /**
     * Get the user t1 returned by the request once the user
     * has logged in
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public get userT2() {
        return localStorage.getItem('userT2');
    }

    /**
     * Get the number returned by the request once the user
     * has logged in
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public get number() {
        return localStorage.getItem('number');
    }

    /**
     * Get the value to know if the user is admin once
     * the user has logged in
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public get isAdmin() {
        return localStorage.getItem('isAdmin');
    }

    /**
     * Get the id login history when the user has been logged in
     * and for checking when start session and when closed it
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    public get historyLogin() {
        return localStorage.getItem('login_history')
    }

    /**
     * Set the tokens for every time the user executes an action
     * and we refresh the tokens we have currently stored
     * 
     * @param object array
     * 
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @platform contractorsbids
     */
    sec_tokens(tokens) {
        localStorage.setItem('userT1', tokens['userS']);
        localStorage.setItem('userT2', tokens['userL']);
        localStorage.setItem('number', tokens['userN']);
    }
}
